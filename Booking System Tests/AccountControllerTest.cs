using System;
using Xunit;
using Computational_Foundry_Booking_System.Controllers;
using Computational_Foundry_Booking_System.Data;
using Moq;
using Microsoft.AspNetCore.Identity;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.View_Models.Accounts;
using Microsoft.AspNetCore.Mvc;
using Xunit.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace Booking_System_Tests {
    public class AccountControllerTest {

        private const string TEST_USERNAME = "test@mail.com";
        private const string TEST_COMPANY_NAME = "Test Compnay";
        private const string TEST_WEBISTE_LINK = "www.google.co.uk";
        private const string TEST_PASSWORD = "Test&123";

        private const string CORRECT_USERNAME = TEST_USERNAME;
        private const string CORRECT_PASSWORD = TEST_PASSWORD;

        private const string INCORRECT_USERNAME = "testing@mail.com";
        private const string INCORRECT_PASSWORD = "Test&12345";

        private readonly PartnerRegisterViewModel _partnerRegisterViewModel =
                new PartnerRegisterViewModel() {
                    Email = TEST_USERNAME,
                    ConfirmEmail = TEST_USERNAME,
                    CompanyName = TEST_COMPANY_NAME,
                    Password = TEST_PASSWORD,
                    ConfirmPassword = TEST_PASSWORD,
                    WebsiteLink = TEST_WEBISTE_LINK
                };

        private readonly ITestOutputHelper _output;

        public AccountControllerTest(ITestOutputHelper output) {
            _output = output;
        }

        [Fact]
        public void TestRegisterSameAccountTwice() {
            //Arrange
            Mock<ApplicationDbContext> mockDbContext =
                TestHelper.GetDbContextMock();

            Mock<UserManager<AppUser>> mockUserManager =
                TestHelper.GetUserManagerMock();

            Mock<SignInManager<AppUser>> mockSignInManager =
                TestHelper.GetSignInManagerMock(mockUserManager.Object);

            //What is returned when the UserManager is attempting to
            //register an account
            IdentityResult failed = new IdentityResult();
            IdentityResult success = IdentityResult.Success;

            //First time success, second time fail (because it will
            //already exist)
            mockUserManager.SetupSequence(x => x.CreateAsync(
                It.IsAny<AppUser>(), It.IsAny<string>()))
                .ReturnsAsync(success)
                .ReturnsAsync(failed);

            AccountController accountController = new AccountController(
                mockDbContext.Object, mockUserManager.Object,
                mockSignInManager.Object);

            //Act

            //First register
            ViewResult result = accountController.Register(
                _partnerRegisterViewModel).Result as ViewResult;

            //Second register
            result = accountController.Register(
                _partnerRegisterViewModel).Result as ViewResult;

            //Assert
            ModelStateEntry entry;
            accountController.ModelState.TryGetValue("", out entry);

            Assert.NotNull(entry);

            string expectedError = AccountController.FAILED_TO_LOGIN_ERR_MSG;

            //Check if the error was outputted
            Assert.Contains(entry.Errors, e =>
                e.ErrorMessage.Equals(expectedError));

            //string errorMessage = entry.Errors.ElementAt(0).ErrorMessage;

            //_output.WriteLine("Model Error: " + errorMessage);
        }
    }
}
