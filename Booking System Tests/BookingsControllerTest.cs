﻿using Computational_Foundry_Booking_System;
using Computational_Foundry_Booking_System.Controllers;
using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Computational_Foundry_Booking_System.Models.Join_Models;
using Computational_Foundry_Booking_System.Models.View_Models.Bookings;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using Xunit;
using Xunit.Abstractions;

namespace Booking_System_Tests {
    public class BookingsControllerTest {

        private static readonly HttpClientHandler clientHandler =
                new HttpClientHandler() {

                    Credentials = new NetworkCredential(
                "cossharepoint\\administrator", "Admin!2019@vm")
                };

        private readonly HttpClient client = new HttpClient(clientHandler);

        private readonly Booking TEST_CREATION_BOOKING = new Booking() {
            Name = "Test Event",
            BeginDateTime = new DateTime(2019, 3, 16, 10, 0, 0),
            EndDateTime = new DateTime(2019, 3, 16, 11, 0, 0),
            Description = "This is a test event."
        };

        private readonly Booking TEST_DELETION_BOOKING = new Booking() {
            Name = "Test Cancel Meeting",
            BeginDateTime = new DateTime(2019, 3, 17, 13, 0, 0),
            EndDateTime = new DateTime(2019, 3, 17, 14, 0, 0),
            Description = "This is a meeting that is being cancelled."
        };

        private readonly Booking TEST_UPDATING_BOOKING = new Booking() {
            Name = "Test To Be Updated Collab",
            BeginDateTime = new DateTime(2019, 3, 18, 15, 0, 0),
            EndDateTime = new DateTime(2019, 3, 18, 16, 0, 0),
            Description = "This is a collaboration that is being updated."
        };

        private readonly NewBookingViewModel VALID_NEW_BOOKING_VIEW_MODEL =
                new NewBookingViewModel() {

            Name = "Test Booking",
            ResourceIds = new string [] {"idk"},
            BeginDateTime = new DateTime(),
            EndDateTime = new DateTime(),
            Description = "Something"
        };

        private readonly ITestOutputHelper _output;

        public BookingsControllerTest(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void TestCreateBookingOnSharePoint() {
            //Arrange
            Mock<ApplicationDbContext> mockDbContext =
                TestHelper.GetDbContextMock();

            Mock<UserManager<AppUser>> mockUserManager =
                TestHelper.GetUserManagerMock();

            Mock<SignInManager<AppUser>> mockSignInManager =
                TestHelper.GetSignInManagerMock(mockUserManager.Object);

            BookingsController bookingsController = new BookingsController(
                mockDbContext.Object, mockUserManager.Object,
                mockSignInManager.Object);

            //Act
            StatusCodeResult result = SharePointHelper.SharePointCreateBooking(
                bookingsController, client, TEST_CREATION_BOOKING).Result;

            //Assert
            //NOTE: It might be better to read data from the SharePoint
            //calendar afterwards to make sure all data was correctly parsed,
            //formatted and accounted for.
            Assert.True(result.StatusCode == (int) HttpStatusCode.OK);

            //Clean
            bookingsController.CancelBooking(TEST_CREATION_BOOKING).Wait();
        }

        [Fact]
        public void TestDeleteBookingOnSharePoint() {
            //Arrange
            Mock<ApplicationDbContext> mockDbContext =
                TestHelper.GetDbContextMock();

            Mock<UserManager<AppUser>> mockUserManager =
                TestHelper.GetUserManagerMock();

            Mock<SignInManager<AppUser>> mockSignInManager =
                TestHelper.GetSignInManagerMock(mockUserManager.Object);

            BookingsController bookingsController = new BookingsController(
                mockDbContext.Object, mockUserManager.Object,
                mockSignInManager.Object);

            SharePointHelper.SharePointCreateBooking(bookingsController,
                client, TEST_DELETION_BOOKING).Wait();

            //Act
            StatusCodeResult result = bookingsController.CancelBooking(
                TEST_DELETION_BOOKING).Result;

            //Assert
            //NOTE: It might be better to check that the deleted data doesn't
            //exist within the SharePoint calendar.
            Assert.True(result.StatusCode == (int) HttpStatusCode.OK);
        }

        [Fact]
        public void TestUpdateBookingOnSharePoint() {
            //Arrange
            Mock<ApplicationDbContext> mockDbContext =
                TestHelper.GetDbContextMock();

            Mock<UserManager<AppUser>> mockUserManager =
                TestHelper.GetUserManagerMock();

            Mock<SignInManager<AppUser>> mockSignInManager =
                TestHelper.GetSignInManagerMock(mockUserManager.Object);

            BookingsController bookingsController = new BookingsController(
                mockDbContext.Object, mockUserManager.Object,
                mockSignInManager.Object);

            SharePointHelper.SharePointCreateBooking(bookingsController,
                client, TEST_UPDATING_BOOKING).Wait();

            TEST_UPDATING_BOOKING.Name = "Test Updated Collab";
            TEST_UPDATING_BOOKING.BeginDateTime = new DateTime(
                2019, 3, 19, 12, 0, 0);

            TEST_UPDATING_BOOKING.EndDateTime = new DateTime(
                2019, 3, 19, 14, 0, 0);

            TEST_UPDATING_BOOKING.Description =
                "This is a collaboration that has been updated.";

            //Act
            //NOTE: It might be better to read data from the SharePoint
            //calendar afterwards to make sure all data was correctly changed.
            StatusCodeResult result = bookingsController.UpdateBooking(
                TEST_UPDATING_BOOKING).Result;

            //Assert
            Assert.True(result.StatusCode == (int) HttpStatusCode.OK);

            //Clean
            bookingsController.CancelBooking(TEST_UPDATING_BOOKING).Wait();
        }

        /*[Fact]
        public void TestValidMakeBooking() {
            //Arrange
            Mock<ApplicationDbContext> mockDbContext =
                TestHelper.GetDbContextMock();

            Mock<UserManager<AppUser>> mockUserManager =
                TestHelper.GetUserManagerMock();

            Mock<SignInManager<AppUser>> mockSignInManager =
                TestHelper.GetSignInManagerMock(mockUserManager.Object);

            Mock<DbSet<Resource>> mockResources = new Mock<DbSet<Resource>>();

            mockResources.Setup(m => m.).Returns(true);

            mockDbContext.Setup(m => m.Resources).Returns(
                mockResources.Object);

            BookingsController bookingsController = new BookingsController(
                mockDbContext.Object, mockUserManager.Object,
                mockSignInManager.Object);

            IActionResult result = bookingsController.MakeBooking(
                VALID_NEW_BOOKING_VIEW_MODEL).Result;

            mockDbContext.Verify(m => m.BookingResources.AddAsync(
                It.IsAny<BookingResource>(), It.IsAny<CancellationToken>()),
                Times.Once);
        }*/

    }
}
