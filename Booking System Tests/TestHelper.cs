﻿using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Booking_System_Tests {
    class TestHelper {

        public static Mock<ApplicationDbContext> GetDbContextMock() {
            DbContextOptions<ApplicationDbContext> options =
                new DbContextOptionsBuilder<ApplicationDbContext>().Options;

            Mock<ApplicationDbContext> mockDbContext =
                new Mock<ApplicationDbContext>(options);

            return mockDbContext;
        }

        public static Mock<UserManager<AppUser>> GetUserManagerMock() {
            Mock<IUserStore<AppUser>> mockUserStore =
                new Mock<IUserStore<AppUser>>();

            Mock<UserManager<AppUser>> mockUserManager =
                new Mock<UserManager<AppUser>>(mockUserStore.Object,
                null, null, null, null, null, null,
                null, null);

            return mockUserManager;
        }

        public static Mock<SignInManager<AppUser>> GetSignInManagerMock(
                UserManager<AppUser> userManager) {

            Mock<IHttpContextAccessor> mockContextAccessor =
                new Mock<IHttpContextAccessor>();

            Mock<IUserClaimsPrincipalFactory<AppUser>> mockClaimsFactory =
                new Mock<IUserClaimsPrincipalFactory<AppUser>>();

            Mock<SignInManager<AppUser>> mockSignInManager =
                new Mock<SignInManager<AppUser>>(
                    userManager, mockContextAccessor.Object,
                    mockClaimsFactory.Object, null, null, null);

            return mockSignInManager;
        }

    }
}
