﻿using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Computational_Foundry_Booking_System.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System {
    public class AppDbInitialiser {

        //Resource groups
        private static readonly ResourceGroup
            TOP_LEFT_TABLE_RESOURCE_GROUP = new ResourceGroup();

        private static readonly ResourceGroup
            BOTTOM_LEFT_TABLE_RESOURCE_GROUP = new ResourceGroup();

        private static readonly ResourceGroup
            TOP_RIGHT_TABLE_RESOURCE_GROUP = new ResourceGroup();

        private static readonly ResourceGroup
            BOTTOM_RIGHT_TABLE_RESOURCE_GROUP = new ResourceGroup();

        private static readonly ResourceGroup LOCKERS_GROUP =
            new ResourceGroup();

        private static readonly ResourceGroup[] RESOURCE_GROUPS = {
            TOP_LEFT_TABLE_RESOURCE_GROUP, BOTTOM_LEFT_TABLE_RESOURCE_GROUP,
            TOP_RIGHT_TABLE_RESOURCE_GROUP, BOTTOM_RIGHT_TABLE_RESOURCE_GROUP,
            LOCKERS_GROUP };

        //Resources

        //Top left table
        private static readonly Resource FIRST_TOP_LEFT_CHAIR =
            new Chair(0, 0, Direction.LEFT,
                TOP_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SECOND_TOP_LEFT_CHAIR =
            new Chair(0, 1, Direction.LEFT,
                TOP_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource THIRD_TOP_LEFT_CHAIR =
            new Chair(0, 2, Direction.LEFT,
                TOP_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FOURTH_TOP_LEFT_CHAIR =
            new Chair(2, 0, Direction.RIGHT,
                TOP_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FIFTH_TOP_LEFT_CHAIR =
            new Chair(2, 1, Direction.RIGHT,
                TOP_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SIXTH_TOP_LEFT_CHAIR =
            new Chair(2, 2, Direction.RIGHT,
                TOP_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource TOP_LEFT_TABLE_TOP =
            new Table(1, 0, TOP_LEFT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_TOP);

        private static readonly Resource TOP_LEFT_TABLE_MIDDLE =
            new Table(1, 1, TOP_LEFT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_MIDDLE);

        private static readonly Resource TOP_LEFT_TABLE_BOTTOM =
            new Table(1, 2, TOP_LEFT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_BOTTOM);

        //Top right table
        private static readonly Resource FIRST_TOP_RIGHT_CHAIR =
            new Chair(4, 0, Direction.LEFT,
                TOP_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SECOND_TOP_RIGHT_CHAIR =
            new Chair(4, 1, Direction.LEFT,
                TOP_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource THIRD_TOP_RIGHT_CHAIR =
            new Chair(4, 2, Direction.LEFT,
                TOP_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FOURTH_TOP_RIGHT_CHAIR =
            new Chair(6, 0, Direction.RIGHT,
                TOP_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FIFTH_TOP_RIGHT_CHAIR =
            new Chair(6, 1, Direction.RIGHT,
                TOP_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SIXTH_TOP_RIGHT_CHAIR =
            new Chair(6, 2, Direction.RIGHT,
                TOP_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource TOP_RIGHT_TABLE_TOP =
            new Table(5, 0, TOP_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_TOP);

        private static readonly Resource TOP_RIGHT_TABLE_MIDDLE =
            new Table(5, 1, TOP_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_MIDDLE);

        private static readonly Resource TOP_RIGHT_TABLE_BOTTOM =
            new Table(5, 2, TOP_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_BOTTOM);

        //Bottom left table
        private static readonly Resource FIRST_BOTTOM_LEFT_CHAIR =
            new Chair(0, 4, Direction.LEFT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SECOND_BOTTOM_LEFT_CHAIR =
            new Chair(0, 5, Direction.LEFT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource THIRD_BOTTOM_LEFT_CHAIR =
            new Chair(0, 6, Direction.LEFT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FOURTH_BOTTOM_LEFT_CHAIR =
            new Chair(0, 7, Direction.LEFT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FIFTH_BOTTOM_LEFT_CHAIR =
            new Chair(2, 4, Direction.RIGHT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SIXTH_BOTTOM_LEFT_CHAIR =
            new Chair(2, 5, Direction.RIGHT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SEVENTH_BOTTOM_LEFT_CHAIR =
            new Chair(2, 6, Direction.RIGHT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource EIGHTH_BOTTOM_LEFT_CHAIR =
            new Chair(2, 7, Direction.RIGHT,
                BOTTOM_LEFT_TABLE_RESOURCE_GROUP);

        private static readonly Resource BOTTOM_LEFT_TABLE_TOP =
            new Table(1, 4, BOTTOM_LEFT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_TOP);

        private static readonly Resource BOTTOM_LEFT_TABLE_FIRST_MIDDLE =
            new Table(1, 5, BOTTOM_LEFT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_MIDDLE);

        private static readonly Resource BOTTOM_LEFT_TABLE_SECOND_MIDDLE =
            new Table(1, 6, BOTTOM_LEFT_TABLE_RESOURCE_GROUP,
            ResourceExtend.SINGLE_MIDDLE);

        private static readonly Resource BOTTOM_LEFT_TABLE_BOTTOM =
            new Table(1, 7, BOTTOM_LEFT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_BOTTOM);

        //Bottom right table
        private static readonly Resource FIRST_BOTTOM_RIGHT_CHAIR =
            new Chair(4, 4, Direction.LEFT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SECOND_BOTTOM_RIGHT_CHAIR =
            new Chair(4, 5, Direction.LEFT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource THIRD_BOTTOM_RIGHT_CHAIR =
            new Chair(4, 6, Direction.LEFT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FOURTH_BOTTOM_RIGHT_CHAIR =
            new Chair(4, 7, Direction.LEFT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource FIFTH_BOTTOM_RIGHT_CHAIR =
            new Chair(6, 4, Direction.RIGHT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SIXTH_BOTTOM_RIGHT_CHAIR =
            new Chair(6, 5, Direction.RIGHT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource SEVENTH_BOTTOM_RIGHT_CHAIR =
            new Chair(6, 6, Direction.RIGHT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource EIGHTH_BOTTOM_RIGHT_CHAIR =
            new Chair(6, 7, Direction.RIGHT,
                BOTTOM_RIGHT_TABLE_RESOURCE_GROUP);

        private static readonly Resource BOTTOM_RIGHT_TABLE_TOP =
            new Table(5, 4, BOTTOM_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_TOP);

        private static readonly Resource BOTTOM_RIGHT_TABLE_FIRST_MIDDLE =
            new Table(5, 5, BOTTOM_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_MIDDLE);

        private static readonly Resource BOTTOM_RIGHT_TABLE_SECOND_MIDDLE =
            new Table(5, 6, BOTTOM_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_MIDDLE);

        private static readonly Resource BOTTOM_RIGHT_TABLE_BOTTOM =
            new Table(5, 7, BOTTOM_RIGHT_TABLE_RESOURCE_GROUP,
                ResourceExtend.SINGLE_BOTTOM);

        //Lockers
        private static readonly Resource LOCKER_LEFT_TOP =
            new Locker(9, 4, LOCKERS_GROUP);

        private static readonly Resource LOCKER_MIDDLE_TOP =
            new Locker(10, 4, LOCKERS_GROUP);

        private static readonly Resource LOCKER_RIGHT_TOP =
            new Locker(11, 4, LOCKERS_GROUP);

        private static readonly Resource LOCKER_LEFT_FIRST_MIDDLE =
            new Locker(9, 5, LOCKERS_GROUP);

        private static readonly Resource LOCKER_MIDDLE_FIRST_MIDDLE =
            new Locker(10, 5, LOCKERS_GROUP);

        private static readonly Resource LOCKER_RIGHT_FIRST_MIDDLE =
            new Locker(11, 5, LOCKERS_GROUP);

        private static readonly Resource LOCKER_LEFT_SECOND_MIDDLE =
            new Locker(9, 6, LOCKERS_GROUP);

        private static readonly Resource LOCKER_MIDDLE_SECOND_MIDDLE =
            new Locker(10, 6, LOCKERS_GROUP);

        private static readonly Resource LOCKER_RIGHT_SECOND_MIDDLE =
            new Locker(11, 6, LOCKERS_GROUP);

        private static readonly Resource LOCKER_LEFT_BOTTOM =
            new Locker(9, 7, LOCKERS_GROUP);

        private static readonly Resource LOCKER_MIDDLE_BOTTOM =
            new Locker(10, 7, LOCKERS_GROUP);

        private static readonly Resource LOCKER_RIGHT_BOTTOM =
            new Locker(11, 7, LOCKERS_GROUP);

        private static readonly Room ROOM = new Room(
            "Computational Foundry Room", RESOURCE_GROUPS);

        /*private const string PARTNER_TEST_USERNAME = "test@mail.com";
        private const string PARTNER_TEST_COMPANY_NAME = "Test Compnay";
        private const string PARTNER_TEST_WEBISTE_LINK = "www.google.co.uk";
        private const string PARTNER_TEST_PASSWORD = "Test&123";

        private const string ADMIN_JOHN_USERNAME = "john@mail.com";
        private const string ADMIN_JOHN_PASSWORD = "John&123";*/

        private const string ADMIN_USERNAME = "CompFoundry@swansea.ac.uk";
        private const string ADMIN_PASSWORD = "oIJb38&unZ257L";

        public static async Task InitialiseAsync(
                ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            AppUserRepository appUserRepository = new AppUserRepository(
                dbContext, userManager, signInManager);

            ResourcesRepository resourcesRepository =
                new ResourcesRepository(dbContext, userManager, signInManager);

            /*await AddPartner(appUserRepository, PARTNER_TEST_USERNAME,
                PARTNER_TEST_COMPANY_NAME, PARTNER_TEST_WEBISTE_LINK,
                PARTNER_TEST_PASSWORD);

            await AddAdmin(appUserRepository, ADMIN_JOHN_USERNAME,
                ADMIN_JOHN_PASSWORD);*/

            //Add admin account to database
            await AddAdmin(appUserRepository, ADMIN_USERNAME,
                ADMIN_PASSWORD);

            //Checks if any rooms are currently stored
            bool exists = await resourcesRepository.DoesRoomExistAsync();

            if(!exists) {
                await resourcesRepository.AddRoomAsync(ROOM);
            }
        }

        private static async Task AddPartner(
                AppUserRepository appUserRepository, string username,
                string companyName, string websiteLink, string password) {

            AppUser user = await appUserRepository.GetUserAsync(
                username);

            if (user == null) {
                IndustryPartner industryPartner = new IndustryPartner() {
                    Approved = true,
                    UserName = username,
                    Email = username,
                    CompanyName = companyName,
                    WebsiteLink = websiteLink
                };

                await appUserRepository.CreateUserAsync(
                    industryPartner, password);
            }
        }

        private static async Task AddAdmin(
                AppUserRepository appUserRepository, string username,
                string password) {

            AppUser user = await appUserRepository.GetUserAsync(
                username);

            if (user == null) {
                Admin admin = new Admin() {
                    UserName = username,
                    Email = username,
                };

                await appUserRepository.CreateUserAsync(admin, password);
            }
        }

    }
}
