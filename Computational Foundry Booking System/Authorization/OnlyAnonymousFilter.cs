﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Authorization {
    public class OnlyAnonymousFilter : IAuthorizationFilter {

        public void OnAuthorization(AuthorizationFilterContext context) {
            if(context.HttpContext.User.Identity.IsAuthenticated) {
                context.Result = new RedirectToActionResult(
                    "Index", "Home", null);
            }
        }

    }
}
