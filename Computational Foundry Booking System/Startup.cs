﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Computational_Foundry_Booking_System.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using System;
using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using static Computational_Foundry_Booking_System.Claims;
using Microsoft.AspNetCore.Authorization;

namespace Computational_Foundry_Booking_System {
    public class Startup {

        private const int LOCKOUT_MINUTES = 10;
        private const int LOCKOUT_ATTEMPTS = 5;

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.Configure<CookiePolicyOptions>(options => {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            LockoutOptions lockoutOptions = new LockoutOptions() {
                AllowedForNewUsers = true,
                DefaultLockoutTimeSpan = TimeSpan.FromMinutes(LOCKOUT_MINUTES),
                MaxFailedAccessAttempts = LOCKOUT_ATTEMPTS
            };

            services.AddIdentity<AppUser, IdentityRole>(options => {
                options.Lockout = lockoutOptions;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddAuthorization(options => {
                foreach(OrClaim claim in CLAIM_REQUIREMENTS_ARR) {
                    AddClaim(options, claim);
                }
            });

            services.ConfigureApplicationCookie(
                options => options.LoginPath = "/Account/Login");

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                IHostingEnvironment env,
                ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            } else {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            dbContext.Database.EnsureCreated();

            AppDbInitialiser.InitialiseAsync(dbContext, userManager,
                signInManager).Wait();

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private void AddClaim(AuthorizationOptions options, OrClaim claim) {
            string[] requirements = claim.Requirements;
            OrRequirements orRequirements = new OrRequirements(requirements);
            options.AddPolicy(claim.Name, p => p.Requirements.Add(
                orRequirements));
        }

    }
}
