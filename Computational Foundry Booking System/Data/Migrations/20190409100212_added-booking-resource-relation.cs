﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Computational_Foundry_Booking_System.Data.Migrations
{
    public partial class addedbookingresourcerelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Booked",
                table: "Resources");

            migrationBuilder.CreateTable(
                name: "BookingResources",
                columns: table => new
                {
                    BookingId = table.Column<string>(nullable: false),
                    ResourceId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingResources", x => new { x.BookingId, x.ResourceId });
                    table.ForeignKey(
                        name: "FK_BookingResources_Bookings_BookingId",
                        column: x => x.BookingId,
                        principalTable: "Bookings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookingResources_Resources_ResourceId",
                        column: x => x.ResourceId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BookingResources_ResourceId",
                table: "BookingResources",
                column: "ResourceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookingResources");

            migrationBuilder.AddColumn<bool>(
                name: "Booked",
                table: "Resources",
                nullable: false,
                defaultValue: false);
        }
    }
}
