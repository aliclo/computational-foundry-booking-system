﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Computational_Foundry_Booking_System.Data.Migrations
{
    public partial class addedresourceblocklocations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HeightBlocks",
                table: "Rooms",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WidthBlocks",
                table: "Rooms",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BlockX",
                table: "Resources",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BlockY",
                table: "Resources",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HeightBlocks",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "WidthBlocks",
                table: "Rooms");

            migrationBuilder.DropColumn(
                name: "BlockX",
                table: "Resources");

            migrationBuilder.DropColumn(
                name: "BlockY",
                table: "Resources");
        }
    }
}
