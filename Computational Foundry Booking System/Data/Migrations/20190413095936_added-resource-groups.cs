﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Computational_Foundry_Booking_System.Data.Migrations
{
    public partial class addedresourcegroups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ResourceGroupId",
                table: "Resources",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ResourceGroups",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    RoomId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResourceGroups", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Resources_ResourceGroupId",
                table: "Resources",
                column: "ResourceGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Resources_ResourceGroups_ResourceGroupId",
                table: "Resources",
                column: "ResourceGroupId",
                principalTable: "ResourceGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Resources_ResourceGroups_ResourceGroupId",
                table: "Resources");

            migrationBuilder.DropTable(
                name: "ResourceGroups");

            migrationBuilder.DropIndex(
                name: "IX_Resources_ResourceGroupId",
                table: "Resources");

            migrationBuilder.DropColumn(
                name: "ResourceGroupId",
                table: "Resources");
        }
    }
}
