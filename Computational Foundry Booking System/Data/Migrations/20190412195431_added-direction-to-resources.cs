﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Computational_Foundry_Booking_System.Data.Migrations
{
    public partial class addeddirectiontoresources : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Direction",
                table: "Resources",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Direction",
                table: "Resources");
        }
    }
}
