﻿using System;
using System.Collections.Generic;
using System.Text;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Computational_Foundry_Booking_System.Models.Join_Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Computational_Foundry_Booking_System.Data {
    public class ApplicationDbContext : IdentityDbContext {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) {
        }

        public DbSet<AppUser> AppUsers { get; set; }

        public DbSet<IndustryPartner> IndustryPartners { get; set; }

        public DbSet<Admin> Admins { get; set; }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<Resource> Resources { get; set; }

        public DbSet<Chair> Chairs { get; set; }

        public DbSet<Table> Tables { get; set; }

        public DbSet<Locker> Lockers { get; set; }

        public DbSet<ResourceGroup> ResourceGroups { get; set; }

        public DbSet<Booking> Bookings { get; set; }

        public virtual DbSet<BookingResource> BookingResources { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);

            builder.Entity<BookingResource>().HasKey(
                br => new { br.BookingId, br.ResourceId });
        }

    }
}
