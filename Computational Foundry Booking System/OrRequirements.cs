﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System {
    public class OrRequirements : AuthorizationHandler<OrRequirements>,
            IAuthorizationRequirement {

        public string[] Requirements { get; }

        public OrRequirements(string[] requirements) {
            Requirements = requirements;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context, OrRequirements requirements) {
            
            foreach(string requirement in requirements.Requirements) {
                if (context.User.HasClaim(c => c.Type == requirement)) {
                    context.Succeed(requirements);
                    return Task.CompletedTask;
                }
            }

            return Task.CompletedTask;
        }
    }
}
