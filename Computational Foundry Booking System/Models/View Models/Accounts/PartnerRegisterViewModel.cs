﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Accounts {
    public class PartnerRegisterViewModel {

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email Address")]
        [Compare("Email", ErrorMessage = "Email addresses don't match")]
        public string ConfirmEmail { get; set; }

        [Required]
        [DataType(DataType.Text), MinLength(2), MaxLength(50)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Passwords don't match")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Url), MaxLength(200)]
        [Display(Name = "Link To Company Website")]
        public string WebsiteLink { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool? Success { get; set; } = false;

        public PartnerRegisterViewModel() {

        }

    }
}
