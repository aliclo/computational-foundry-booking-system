﻿using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Accounts {
    public class AccountApprovalViewModel {

        public AwaitingAccountViewModel[] AwaitingAccounts { get; set; }

        public AccountApprovalViewModel(IndustryPartner[] accounts) {
            AwaitingAccounts = new AwaitingAccountViewModel[accounts.Length];

            for(int i = 0; i < accounts.Length; i++) {
                AwaitingAccounts[i] = new AwaitingAccountViewModel(
                    accounts[i]);
            }
        }

    }
}
