﻿using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Accounts {
    public class AwaitingAccountViewModel {

        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string WebsiteLink { get; set; }

        public AwaitingAccountViewModel(IndustryPartner account) {
            Email = account.Email;
            CompanyName = account.CompanyName;
            WebsiteLink = account.WebsiteLink;
        }

    }
}
