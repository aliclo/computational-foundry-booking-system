﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public abstract class ResourceViewModel {

        public int BlockX { get; set; }
        public int BlockY { get; set; }
        public string Id { get; set; }
        public string GroupId { get; set; }
        public ResourceGroupViewModel ResourceGroup { get; set; }
        public string CurrentIconPath { get; set; }

        public ResourceViewModel(Resource resource,
                ResourceGroupViewModel resourceGroup, string groupId) {

            BlockX = resource.BlockX;
            BlockY = resource.BlockY;
            Id = resource.Id;
            GroupId = groupId;
            ResourceGroup = resourceGroup;
        }

        public abstract string GetUnselectedResourceIcon();

        public abstract string GetSelectedResourceIcon();

        public abstract string GetUnselectedBookedResourceIcon();

        public abstract string GetSelectedBookedResourceIcon();

        public abstract string GetResourceType();

        public abstract RoomBookingViewModel GetBooking();

        public abstract void SetBooked(RoomBookingViewModel booking);

        public abstract bool IsSelectable();

        public abstract bool IsBooked();

        public abstract bool IsSelected();

        public abstract void InitialiseCurrentIconPath();

        public abstract string GetHoverPopupHtml();

    }
}