﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class ResourcesSelectionViewModel {

        public DateTime SelectedDateTime { get; set; }
        public string[] ResourceIds { get; set; }

    }
}
