﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public abstract class SelectableResourceViewModel : ResourceViewModel {

        public bool Selected { get; set; }
        public bool Booked { get; set; } = false;
        public RoomBookingViewModel Booking { get; set; }

        public SelectableResourceViewModel(Resource resource,
                ResourceGroupViewModel resourceGroup, string groupId,
                bool selected) : base(resource, resourceGroup, groupId) {

            Selected = selected;
        }

        public override RoomBookingViewModel GetBooking() {
            return Booking;
        }

        public override void SetBooked(RoomBookingViewModel booking) {
            Booked = true;

            if (Selected) {
                CurrentIconPath = GetSelectedBookedResourceIcon();
            } else {
                CurrentIconPath = GetUnselectedBookedResourceIcon();
            }

            Booking = booking;

            ResourceGroup.ResourceBooked(this);
        }

        public override bool IsSelectable() {
            return true;
        }

        public override bool IsBooked() {
            return Booked;
        }

        public override bool IsSelected() {
            return Selected;
        }

        public override void InitialiseCurrentIconPath() {
            if (Selected) {
                CurrentIconPath = GetSelectedResourceIcon();
            } else {
                CurrentIconPath = GetUnselectedResourceIcon();
            }
        }

        public override string GetHoverPopupHtml() {
            if (IsBooked()) {
                return GetBookedHoverPopupHtml();
            } else {
                return GetNotBookedHoverPopupHtml();
            }
        }

        private string GetBookedHoverPopupHtml() {
            string companyName = GetBooking().CompanyName;
            string name = GetBooking().Name;
            string startStr = GetBooking().StartStr;
            string endStr = GetBooking().EndStr;
            string description = GetBooking().Description;

            return
                "<div class='booking-hover-popup'>" +
                "  <p class='popup-large-text'>" + GetResourceType() + "</p>" +
                "  <p class='popup-text'>Booked</p>" +
                "  <div class='popup-hr'></div>" +
                "  <p class='popup-large-text'>" + companyName + "</p>" +
                "  <p class='popup-text'>" + name + "</p>" +
                "  <div class='popup-hr'></div>" +
                "  <p class='popup-text'>From " + startStr + "</p>" +
                "  <p class='popup-text'>To " + endStr + "</p>" +
                "  <div class='popup-hr'></div>" +
                "  <p class='popup-text'>" + description + "</p>" +
                "</div>";
        }

        private string GetNotBookedHoverPopupHtml() {
            return
                "<div class='booking-hover-popup'>" +
                "  <p class='popup-large-text'>" + GetResourceType() + "</p>" +
                "  <p class='popup-text'>Free</p>" +
                "</div>";
        }

    }
}
