﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class BookingTimeAreaViewModel {

        private List<TimeIndex> timeIndices = new List<TimeIndex>();

        public int GridColumn { get; set; }
        public int GridStartRow { get; set; }
        public int GridEndRow { get; set; }
        public int Count { get; set; }
        public int NumOfColumns { get; set; }
        public List<WeeklyBookingBlockViewModel> bookingBlocks =
            new List<WeeklyBookingBlockViewModel>();

        public BookingTimeAreaViewModel(WeeklyBookingBlockViewModel booking) {

            GridColumn = booking.GridColumn;
            GridStartRow = booking.GridStartRow;
            GridEndRow = booking.GridEndRow;
            bookingBlocks.Add(booking);
            Count++;
        }

        public void IncludeArea(BookingTimeAreaViewModel bookingArea) {
            foreach(WeeklyBookingBlockViewModel bookingBlock
                    in bookingArea.bookingBlocks) {

                bookingBlocks.Add(bookingBlock);

                if (bookingBlock.GridStartRow < GridStartRow) {
                    GridStartRow = bookingBlock.GridStartRow;
                }

                if(bookingBlock.GridEndRow > GridEndRow) {
                    GridEndRow = bookingBlock.GridEndRow;
                }
            }

            Count += bookingArea.bookingBlocks.Count;

            /*bookingArea.bookingBlocks = bookingBlocks;
            bookingArea.GridColumn = GridColumn;
            bookingArea.GridStartRow = GridStartRow;
            bookingArea.GridEndRow = GridEndRow;
            bookingArea.Count = Count;*/
        }

        public void AddTo(BookingTimeAreaViewModel[][] bookingAreasGrid,
                int dayIndex, int hourIndex) {

            timeIndices.Add(new TimeIndex(dayIndex, hourIndex));
            bookingAreasGrid[dayIndex][hourIndex] = this;
        }

        public void ReplaceWith(BookingTimeAreaViewModel[][] bookingAreasGrid,
                BookingTimeAreaViewModel bookingArea) {

            foreach(TimeIndex timeIndex in timeIndices) {
                int dayIndex = timeIndex.DayIndex;
                int hourIndex = timeIndex.HourIndex;
                bookingArea.AddTo(bookingAreasGrid, dayIndex, hourIndex);
            }
        }

        public void PositionColumns() {
            //Worst case time complexity: O(n^3),
            //where n is the number of bookings
            /*bool[][] blocksTaken = new bool[Count][];

            int numOfRows = GridEndRow - GridStartRow + 1;

            for(int i = 0; i < Count; i++) {
                blocksTaken[i] = new bool[numOfRows];
            }

            foreach(WeeklyBookingBlockViewModel block in bookingBlocks) {
                int findColumn = -1;
                bool collision = true;

                while (collision) {
                    findColumn++;
                    collision = false;
                    int rowIndex = block.GridStartRow;

                    while (rowIndex <= block.GridEndRow && !collision) {
                        collision = blocksTaken[findColumn][rowIndex];
                        rowIndex++;
                    }
                }

                for(int rowIndex = block.GridStartRow;
                        rowIndex <= block.GridEndRow; rowIndex++) {

                    blocksTaken[findColumn][rowIndex] = true;
                }

                block.GridInternalColumn = findColumn + 1;
            }*/

            //Worst case time complexity: O(n^2),
            //where n is the number of bookings

            //All blocks start from right most column to sink down to the
            //left side of the grid
            for(int i = 0; i < bookingBlocks.Count; i++) {
                bookingBlocks[i].GridInternalColumn = Count;
            }

            //Find column position for each block
            for(int i = 0; i < bookingBlocks.Count; i++) {
                WeeklyBookingBlockViewModel booking = bookingBlocks[i];
                bool[] columnTaken = new bool[Count];

                //Find each of the other blocks that share the same rows

                //Blocks indexed to the left of the current block
                for (int j = 0; j < i; j++) {
                    WeeklyBookingBlockViewModel other = bookingBlocks[j];

                    if (!((other.GridStartRow <= booking.GridStartRow &&
                            other.GridEndRow <= booking.GridStartRow) ||
                            (other.GridStartRow >= booking.GridEndRow &&
                            other.GridEndRow >= booking.GridEndRow))) {

                        columnTaken[other.GridInternalColumn-1] = true;
                    }
                }

                //Blocks indexed to the right of the current block
                for (int j = i+1; j < Count; j++) {
                    WeeklyBookingBlockViewModel other = bookingBlocks[j];

                    if (!((other.GridStartRow <= booking.GridStartRow &&
                            other.GridEndRow <= booking.GridStartRow) ||
                            (other.GridStartRow >= booking.GridEndRow &&
                            other.GridEndRow >= booking.GridEndRow))) {

                        columnTaken[other.GridInternalColumn - 1] = true;
                    }
                }

                int ci = 0;
                bool found = false;

                while(!found) {
                    if (columnTaken[ci]) {
                        ci++;
                    } else {
                        found = true;
                    }
                }

                int columnPlacement = ci + 1;
                booking.GridInternalColumn = columnPlacement;

                NumOfColumns = Math.Max(NumOfColumns, columnPlacement);
            }

            /*WeeklyBookingBlockViewModel[][] bookingsGrid =
                new WeeklyBookingBlockViewModel[Count][];

            int numOfRows = GridEndRow - GridStartRow + 1;

            for(int i = 0; i < Count; i++) {
                bookingsGrid[i] = new WeeklyBookingBlockViewModel[numOfRows];
            }

            foreach(WeeklyBookingBlockViewModel booking in bookingBlocks) {
                int gridRowIndex = booking.grid
            }*/
        }

        class TimeIndex {

            public int DayIndex { get; set; }
            public int HourIndex { get; set; }

            public TimeIndex(int dayIndex, int hourIndex) {
                this.DayIndex = dayIndex;
                this.HourIndex = hourIndex;
            }

        }

        /*public void AddBooking(WeeklyBookingViewModel booking) {
            bookings.Add(booking);

            if(StartTime == null) {
                StartTime = booking.BeginDateTime;
                EndTime = booking.EndDateTime;
            } else {
                if(booking.BeginDateTime < StartTime) {
                    StartTime = booking.BeginDateTime;
                }

                if(booking.EndDateTime > EndTime) {
                    EndTime = booking.EndDateTime;
                }
            }
        }*/

    }
}
