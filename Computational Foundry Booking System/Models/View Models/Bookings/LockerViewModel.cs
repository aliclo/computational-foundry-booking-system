﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class LockerViewModel : SelectableResourceViewModel {

        private const string SELECTED =
            "/images/locker-selected.svg";

        private const string UNSELECTED =
            "/images/locker-unselected.svg";

        private const string SELECTED_BOOKED =
            "/images/locker-selected-booked.svg";

        private const string UNSELECTED_BOOKED =
            "/images/locker-unselected-booked.svg";

        private const string RESOURCE_TYPE = "Locker";

        public LockerViewModel(Locker locker,
                ResourceGroupViewModel resourceGroup, string groupId,
                bool selected) : base(locker, resourceGroup,
                groupId, selected) {

            InitialiseCurrentIconPath();
        }

        public override string GetSelectedBookedResourceIcon() {
            return SELECTED_BOOKED;
        }

        public override string GetSelectedResourceIcon() {
            return SELECTED;
        }

        public override string GetUnselectedBookedResourceIcon() {
            return UNSELECTED_BOOKED;
        }

        public override string GetUnselectedResourceIcon() {
            return UNSELECTED;
        }

        public override string GetResourceType() {
            return RESOURCE_TYPE;
        }

    }
}
