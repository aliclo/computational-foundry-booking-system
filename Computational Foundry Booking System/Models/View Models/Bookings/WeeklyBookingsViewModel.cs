﻿using Computational_Foundry_Booking_System.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class WeeklyBookingsViewModel {

        private const int GRID_ROW_START = 2;
        private const int GRID_COLUMN_START = 2;

        private readonly BookingTimeAreaViewModel[][] bookingAreasGrid;
        private readonly DateTime weekEndDate;
        private readonly int gridRowEnd;
        private readonly int gridColumnEnd;
        private readonly int endSlotHourEnd;

        public DateTime WeekDate { get; set; }
        public DateTime StartWeekTime { get; set; }
        public int EndDay { get; set; }
        public int StartHour { get; set; }
        public int EndHour { get; set; }
        public HashSet<BookingTimeAreaViewModel> BookingAreas { get; set; } =
            new HashSet<BookingTimeAreaViewModel>();

        /*public List<WeeklyBookingViewModel> UserBookings { get; set; }
        public List<WeeklyBookingViewModel> OtherBookings { get; set; }*/

        public WeeklyBookingsViewModel(DateTime weekDate,
                DayOfWeek endDay, int startHour, int endHour) {

            WeekDate = weekDate;
            StartWeekTime = weekDate.AddHours(startHour);
            EndDay = (int) endDay;
            StartHour = startHour;
            EndHour = endHour;
            endSlotHourEnd = EndHour + 1;

            weekEndDate = WeekDate.AddDays(BookingsController.DAYS_IN_WEEK);

            int numOfDays = EndDay-((int) DayOfWeek.Monday)+1;
            int numOfHours = EndHour-StartHour+1;

            bookingAreasGrid = new BookingTimeAreaViewModel[numOfDays][];

            for(int i = 0; i < numOfDays; i++) {
                bookingAreasGrid[i] =
                    new BookingTimeAreaViewModel[numOfHours];
            }

            gridRowEnd = GRID_ROW_START + (endSlotHourEnd - StartHour);
            gridColumnEnd = GRID_COLUMN_START
                + (EndDay - ((int) DayOfWeek.Monday) + 1);
        }

        public void AddBookings(List<WeeklyBookingViewModel> bookings) {
            foreach(WeeklyBookingViewModel booking in bookings) {
                AddBooking(booking);
            }

            foreach(BookingTimeAreaViewModel[] bookingTimeAreas
                    in bookingAreasGrid) {

                AddBookingAreas(bookingTimeAreas);
            }

            foreach(BookingTimeAreaViewModel bookingArea in BookingAreas) {
                bookingArea.PositionColumns();
            }
        }

        private void AddBookingAreas(BookingTimeAreaViewModel[] bookingAreas) {
            foreach (BookingTimeAreaViewModel bookingArea in bookingAreas) {
                if (bookingArea != null) {
                    BookingAreas.Add(bookingArea);
                }
            }
        }

        private void AddBooking(WeeklyBookingViewModel booking) {
            DateTime startTime = booking.BeginDateTime;
            DateTime endTime = booking.EndDateTime;

            DateTime startDateTime = booking.BeginDateTime;
            int startDay;
            int endDay;
            int startHour;
            int endHour;

            /*
             * Set time and day to very start if the start of the booking
             * is before the current week
             */
            if(startTime < WeekDate) {
                startDay = (int)DayOfWeek.Monday;
                startHour = StartHour;
                startDateTime = StartWeekTime;
            } else {
                startDay = (int)startTime.DayOfWeek;
                startHour = startTime.Hour;
            }

            /*
             * Set time and day to very end if the end of the booking
             * is after the current week
             */
            if (endTime > weekEndDate) {
                endDay = EndDay;
                endHour = endSlotHourEnd;
            } else {
                endDay = (int) endTime.DayOfWeek;
                endHour = endTime.Hour;
            }

            PlaceBooking(booking, startDateTime, startDay, endDay,
                startHour, endHour);
        }

        private void PlaceBooking(WeeklyBookingViewModel booking,
                DateTime startDateTime, int startDay, int endDay,
                int startHour, int endHour) {

            //If the booking starts and ends on the same day, just place the
            //booking for that day
            if (startDay == endDay) {
                int gridColumn = startDay - ((int)DayOfWeek.Monday)
                    + GRID_COLUMN_START;

                int gridStartRow = startHour - StartHour + GRID_ROW_START;
                int gridEndRow = endHour - StartHour + GRID_ROW_START;

                WeeklyBookingBlockViewModel bookingBlock =
                    new WeeklyBookingBlockViewModel(booking,
                    startDateTime, gridColumn, gridStartRow,
                    gridEndRow);

                PlaceBookingBlock(bookingBlock, startDay, startHour, endHour);
            } else {
                int gridColumn = startDay - ((int)DayOfWeek.Monday)
                    + GRID_COLUMN_START;

                int gridStartRow = startHour - StartHour + GRID_ROW_START;

                WeeklyBookingBlockViewModel bookingBlock =
                    new WeeklyBookingBlockViewModel(booking,
                    startDateTime, gridColumn,
                    gridStartRow, gridRowEnd);

                //Place the booking for the first day where it starts
                PlaceBookingBlock(bookingBlock, startDay,
                    startHour, endSlotHourEnd);

                DateTime nextStart = startDateTime.Date;
                nextStart = nextStart.AddHours(StartHour).AddDays(1);

                //Place the booking for the following days where it continues
                for (int d = startDay + 1; d < endDay; d++) {
                    gridColumn++;

                    bookingBlock = new WeeklyBookingBlockViewModel(booking,
                        nextStart, gridColumn, GRID_ROW_START, gridRowEnd);

                    PlaceBookingBlock(bookingBlock, d, StartHour,
                        endSlotHourEnd);

                    nextStart = nextStart.AddDays(1);
                }

                gridColumn++;
                int gridEndRow = endHour - StartHour + GRID_ROW_START;

                bookingBlock = new WeeklyBookingBlockViewModel(booking,
                    nextStart, gridColumn, GRID_ROW_START, gridEndRow);

                //Place the booking for the last day where it ends
                PlaceBookingBlock(bookingBlock, endDay, StartHour, endHour);
            }
        }

        private void PlaceBookingBlock(
                WeeklyBookingBlockViewModel bookingBlock, int day,
                int startHour, int endHour) {

            BookingTimeAreaViewModel bookingArea =
                new BookingTimeAreaViewModel(bookingBlock);

            HashSet<BookingTimeAreaViewModel> encounteredAreas =
                new HashSet<BookingTimeAreaViewModel>();

            /*
             * Exclude the end hour of the booking as we don't want to
             * include the last hour itself as this includes the hour
             * after the last time slot of the booking, e.g. start hour
             * at 17:00 and end hour at 18:00 for the end of the time
             * slot at 17:00.
             */
            for (int h = startHour; h < endHour; h++) {
                PlaceBookingArea(bookingArea, encounteredAreas, day, h);
            }
        }

        private void PlaceBookingArea(
                BookingTimeAreaViewModel bookingArea,
                HashSet<BookingTimeAreaViewModel> encounteredAreas, int day,
                int hour) {

            int dayIndex = day - ((int) DayOfWeek.Monday);
            int hourIndex = hour - StartHour;

            BookingTimeAreaViewModel bookingEncounter =
                bookingAreasGrid[dayIndex][hourIndex];

            //Check if we have encountered a booking we haven't
            //encountered before
            if (bookingEncounter == null ||
                    encounteredAreas.Contains(bookingEncounter)) {

                bookingArea.AddTo(bookingAreasGrid, dayIndex, hourIndex);
            } else {
                bookingArea.IncludeArea(bookingEncounter);
                encounteredAreas.Add(bookingEncounter);
                bookingEncounter.ReplaceWith(bookingAreasGrid, bookingArea);
            }
        }

    }
}
