﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class BookingFormViewModel {

        public NewBookingViewModel NewBooking { get; set; }
        public int EndDay { get; set; }
        public int StartHour { get; set; }
        public int EndHour { get; set; }

        public BookingFormViewModel(NewBookingViewModel newBooking,
                DayOfWeek endDay, int startHour, int endHour) {

            this.NewBooking = newBooking;
            this.EndDay = (int) endDay;
            this.StartHour = startHour;
            this.EndHour = endHour;
        }

    }
}
