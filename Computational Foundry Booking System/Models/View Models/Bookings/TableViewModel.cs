﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class TableViewModel : UnselectableResourceViewModel {

        private const string SINGLE_MIDDLE_SELECTED =
            "/images/middle-single-table-selected.svg";

        private const string SINGLE_MIDDLE_UNSELECTED =
            "/images/middle-single-table-unselected.svg";

        private const string SINGLE_MIDDLE_SELECTED_BOOKED =
            "/images/middle-single-table-selected-booked.svg";

        private const string SINGLE_MIDDLE_UNSELECTED_BOOKED =
            "/images/middle-single-table-unselected-booked.svg";

        private const string SINGLE_TOP_SELECTED =
            "/images/top-end-table-selected.svg";

        private const string SINGLE_TOP_UNSELECTED =
            "/images/top-end-table-unselected.svg";

        private const string SINGLE_TOP_SELECTED_BOOKED =
            "/images/top-end-table-selected-booked.svg";

        private const string SINGLE_TOP_UNSELECTED_BOOKED =
            "/images/top-end-table-unselected-booked.svg";

        private const string SINGLE_BOTTOM_SELECTED =
            "/images/bottom-end-table-selected.svg";

        private const string SINGLE_BOTTOM_UNSELECTED =
            "/images/bottom-end-table-unselected.svg";

        private const string SINGLE_BOTTOM_SELECTED_BOOKED =
            "/images/bottom-end-table-selected-booked.svg";

        private const string SINGLE_BOTTOM_UNSELECTED_BOOKED =
            "/images/bottom-end-table-unselected-booked.svg";

        private const string RESOURCE_TYPE = "Table";

        private string _selectedIconPath;
        private string _unselectedIconPath;
        private string _selectedBookedIconPath;
        private string _unselectedBookedIconPath;

        public TableViewModel(Table table,
                ResourceGroupViewModel resourceGroup, string groupId)
                : base(table, resourceGroup, groupId) {

            switch (table.ExtendType) {
                case ResourceExtend.SINGLE_MIDDLE:
                    SetSingleMiddleIconPaths();
                    break;
                case ResourceExtend.SINGLE_BOTTOM:
                    SetSingleBottomIconPaths();
                    break;
                default:
                    SetSingleTopIconPaths();
                    break;
            }

            InitialiseCurrentIconPath();
        }

        private void SetSingleMiddleIconPaths() {
            _selectedIconPath = SINGLE_MIDDLE_SELECTED;
            _unselectedIconPath = SINGLE_MIDDLE_UNSELECTED;
            _selectedBookedIconPath = SINGLE_MIDDLE_SELECTED_BOOKED;
            _unselectedBookedIconPath = SINGLE_MIDDLE_UNSELECTED_BOOKED;
        }

        private void SetSingleBottomIconPaths() {
            _selectedIconPath = SINGLE_BOTTOM_SELECTED;
            _unselectedIconPath = SINGLE_BOTTOM_UNSELECTED;
            _selectedBookedIconPath = SINGLE_BOTTOM_SELECTED_BOOKED;
            _unselectedBookedIconPath = SINGLE_BOTTOM_UNSELECTED_BOOKED;
        }

        private void SetSingleTopIconPaths() {
            _selectedIconPath = SINGLE_TOP_SELECTED;
            _unselectedIconPath = SINGLE_TOP_UNSELECTED;
            _selectedBookedIconPath = SINGLE_TOP_SELECTED_BOOKED;
            _unselectedBookedIconPath = SINGLE_TOP_UNSELECTED_BOOKED;
        }

        public override string GetSelectedBookedResourceIcon() {
            return _selectedBookedIconPath;
        }

        public override string GetSelectedResourceIcon() {
            return _selectedIconPath;
        }

        public override string GetUnselectedBookedResourceIcon() {
            return _unselectedBookedIconPath;
        }

        public override string GetUnselectedResourceIcon() {
            return _unselectedIconPath;
        }

        public override string GetResourceType() {
            return RESOURCE_TYPE;
        }

    }
}
