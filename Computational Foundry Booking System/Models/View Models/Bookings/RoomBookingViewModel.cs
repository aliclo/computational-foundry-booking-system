﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class RoomBookingViewModel {

        public string Id { get; set; }

        public string Name { get; set; }
        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string StartStr { get; }
        public string EndStr { get; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public bool UserBooking { get; set; }
        public List<string> ResourceIds { get; set; }

        public RoomBookingViewModel(Booking booking, bool userBooking) {
            this.Id = booking.Id;
            this.Name = booking.Name;
            this.BeginDateTime = booking.BeginDateTime;
            this.EndDateTime = booking.EndDateTime;
            this.Description = booking.Description;
            this.CompanyName = booking.User.CompanyName;
            this.UserBooking = userBooking;

            StartStr = BeginDateTime.ToString("d MMM hh:00");
            EndStr = EndDateTime.ToString("d MMM hh:00");

            ResourceIds = new List<string>();

            foreach(Resource resource in booking.Resources) {
                ResourceIds.Add(resource.Id);
            }
        }

    }
}
