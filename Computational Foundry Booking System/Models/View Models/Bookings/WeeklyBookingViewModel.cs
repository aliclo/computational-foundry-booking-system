﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Computational_Foundry_Booking_System.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class WeeklyBookingViewModel {

        public string Id { get; set; }

        public string Name { get; set; }
        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public bool UserBooking { get; set; }
        public int NumOfBookedChairs { get; set; }
        public int NumOfBookedLockers { get; set; }

        public WeeklyBookingViewModel(Booking booking, bool userBooking) {
            this.Id = booking.Id;
            this.Name = booking.Name;
            this.BeginDateTime = booking.BeginDateTime;
            this.EndDateTime = booking.EndDateTime;
            this.Description = booking.Description;
            this.CompanyName = booking.User.CompanyName;
            this.UserBooking = userBooking;
        }

        public async Task Initialise(Booking booking,
                ResourcesRepository resourcesRepository) {

            NumOfBookedChairs = await resourcesRepository
                .GetNumBookedChairs(booking);

            NumOfBookedLockers = await resourcesRepository
                .GetNumBookedLockers(booking);
        }

    }
}
