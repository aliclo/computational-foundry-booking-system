﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class NewBookingViewModel {

        [Required]
        [DataType(DataType.Text)]
        [Display(Name="Name")]
        public string Name { get; set; }

        [Required]
        [HiddenInput(DisplayValue = false)]
        [DataType(DataType.Text)]
        [Display(Name = "Resources")]
        public string[] ResourceIds { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Start Time")]
        public DateTime BeginDateTime { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "End Time")]
        public DateTime EndDateTime { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public NewBookingViewModel() {

        }

        public NewBookingViewModel(
                ResourcesSelectionViewModel resourcesSelection) {

            string[] resourceIds = resourcesSelection.ResourceIds;

            ResourceIds = resourceIds;

            /*for(int i = 0; i < resourceIds.Length; i++) {
                ResourceIds += resourceIds[i] + ";";
            }*/

            this.BeginDateTime = resourcesSelection.SelectedDateTime;
            this.EndDateTime = resourcesSelection.SelectedDateTime
                .AddHours(1);
        }

    }
}
