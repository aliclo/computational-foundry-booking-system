﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public abstract class UnselectableResourceViewModel : ResourceViewModel {

        private bool _fullyBooked = false;

        public UnselectableResourceViewModel(Resource resource,
                ResourceGroupViewModel resourceGroup, string groupId) :
                base(resource, resourceGroup, groupId) {

        }

        public override RoomBookingViewModel GetBooking() {
            return null;
        }

        public override void SetBooked(RoomBookingViewModel booking) {
            CurrentIconPath = GetUnselectedBookedResourceIcon();
            _fullyBooked = true;
        }

        public override bool IsSelectable() {
            return false;
        }

        public override bool IsBooked() {
            return _fullyBooked;
        }

        public override bool IsSelected() {
            return false;
        }

        public override void InitialiseCurrentIconPath() {
            CurrentIconPath = GetUnselectedResourceIcon();
        }

        public override string GetHoverPopupHtml() {
            if (_fullyBooked) {
                return GetFullyBookedHoverPopupHtml();
            } else {
                return GetNotFullyBookedHoverPopupHtml();
            }
        }

        public string GetFullyBookedHoverPopupHtml() {
            return
                "<div class='booking-hover-popup'>" +
                "  <p class='popup-large-text'>" + GetResourceType() + "</p>" +
                "  <p class='popup-text'>Fully Booked</p>" +
                "</div>";
        }

        public string GetNotFullyBookedHoverPopupHtml() {
            return
                "<div class='booking-hover-popup'>" +
                "  <p class='popup-large-text'>" + GetResourceType() + "</p>" +
                "  <p class='popup-text'>Free</p>" +
                "</div>";
        }

    }
}
