﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class ResourceGroupViewModel {

        public string SelectableId { get; set; }
        public string UnselectableId { get; set; }

        public List<ResourceViewModel> SelectableResources { get; set; } =
            new List<ResourceViewModel>();

        public List<ResourceViewModel> UnselectableResources { get; set; } =
            new List<ResourceViewModel>();

        private int _bookedResources = 0;

        //A counter can be used instead if it is guaranteed that each
        //resource only notifies once
        /*private readonly HashSet<ResourceViewModel> _bookedResources =
            new HashSet<ResourceViewModel>();*/

        /*public string SelectableResourcesStr { get; set; }
        public string UnselectableResourcesStr { get; set; }*/

        public ResourceGroupViewModel(ResourceGroup resourceGroup) {
            SelectableId = "S-" + resourceGroup.Id;
            UnselectableId = "U-" + resourceGroup.Id;

            List<Resource> selectableResources =
                resourceGroup.SelectableResources;

            List<Resource> unselectableResources =
                resourceGroup.UnselectableResources;

            /*SelectableResourcesStr = GetResourcesString(
                selectableResources);

            UnselectableResourcesStr = GetResourcesString(
                unselectableResources);*/

            foreach (Resource resource in selectableResources) {
                SelectableResources.Add(resource.GetResourceRoomViewModel(
                    this, SelectableId, false));
            }

            foreach (Resource resource in unselectableResources) {
                UnselectableResources.Add(resource.GetResourceRoomViewModel(
                    this, UnselectableId, false));
            }
        }

        public void ResourceBooked(ResourceViewModel resource) {
            _bookedResources++;

            if(_bookedResources == SelectableResources.Count) {
                foreach(ResourceViewModel unselectableResource
                        in UnselectableResources) {

                    unselectableResource.SetBooked(null);
                }
            }
        }

        /*private string GetResourcesString(List<Resource> resources) {
            string resourcesListStr = "[";
            
            if (resources.Any()) {
                resourcesListStr += resources[0].Id;

                for (int i = 1; i < resources.Count; i++) {
                    resourcesListStr += ",\'" + resources[i].Id + '\'';
                }
            }

            resourcesListStr += "]";

            return resourcesListStr;
        }*/

    }
}
