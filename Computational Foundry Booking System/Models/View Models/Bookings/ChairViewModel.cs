﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class ChairViewModel : SelectableResourceViewModel {

        private const string LEFT_SELECTED =
            "/images/left-chair-selected-thicker.svg";

        private const string LEFT_UNSELECTED =
            "/images/left-chair-unselected-thicker.svg";

        private const string LEFT_SELECTED_BOOKED =
            "/images/left-chair-selected-booked-thicker.svg";

        private const string LEFT_UNSELECTED_BOOKED =
            "/images/left-chair-unselected-booked-thicker.svg";

        private const string RIGHT_SELECTED =
            "/images/right-chair-selected-thicker.svg";

        private const string RIGHT_UNSELECTED =
            "/images/right-chair-unselected-thicker.svg";

        private const string RIGHT_SELECTED_BOOKED =
            "/images/right-chair-selected-booked-thicker.svg";

        private const string RIGHT_UNSELECTED_BOOKED =
            "/images/right-chair-unselected-booked-thicker.svg";

        private const string RESOURCE_TYPE = "Chair";

        private string _selectedIconPath;
        private string _unselectedIconPath;
        private string _selectedBookedIconPath;
        private string _unselectedBookedIconPath;

        public ChairViewModel(Chair chair,
                ResourceGroupViewModel resourceGroup, string groupId,
                bool selected) : base(chair, resourceGroup,
                groupId, selected) {

            switch (chair.Direction) {
                case Direction.LEFT:
                    SetLeftIconPaths();
                    break;
                default:
                    SetRightIconPaths();
                    break;
            }

            InitialiseCurrentIconPath();
        }

        private void SetLeftIconPaths() {
            _selectedIconPath = LEFT_SELECTED;
            _unselectedIconPath = LEFT_UNSELECTED;
            _selectedBookedIconPath = LEFT_SELECTED_BOOKED;
            _unselectedBookedIconPath = LEFT_UNSELECTED_BOOKED;
        }

        private void SetRightIconPaths() {
            _selectedIconPath = RIGHT_SELECTED;
            _unselectedIconPath = RIGHT_UNSELECTED;
            _selectedBookedIconPath = RIGHT_SELECTED_BOOKED;
            _unselectedBookedIconPath = RIGHT_UNSELECTED_BOOKED;
        }

        public override string GetSelectedBookedResourceIcon() {
            return _selectedBookedIconPath;
        }

        public override string GetSelectedResourceIcon() {
            return _selectedIconPath;
        }

        public override string GetUnselectedBookedResourceIcon() {
            return _unselectedBookedIconPath;
        }

        public override string GetUnselectedResourceIcon() {
            return _unselectedIconPath;
        }

        public override string GetResourceType() {
            return RESOURCE_TYPE;
        }

    }
}
