﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class WeeklyBookingBlockViewModel {

        public WeeklyBookingViewModel Booking { get; set; }
        public DateTime StartDateTime { get; set; }
        public int GridColumn { get; set; }
        public int GridInternalColumn { get; set; }
        public int GridStartRow { get; set; }
        public int GridEndRow { get; set; }

        public WeeklyBookingBlockViewModel(
                WeeklyBookingViewModel weeklyBooking, DateTime startDateTime,
                int gridColumn, int gridStartRow, int gridEndRow) {

            this.Booking = weeklyBooking;
            this.StartDateTime = startDateTime;
            this.GridColumn = gridColumn;
            this.GridStartRow = gridStartRow;
            this.GridEndRow = gridEndRow;
        }

    }
}
