﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.View_Models.Bookings {
    public class RoomBookingsViewModel {

        public DateTime SelectedDateTime { get; set; }
        public DateTime NextDay { get; set; }
        public DateTime PreviousDay { get; set; }
        public DateTime NextHour { get; set; }
        public DateTime PreviousHour { get; set; }
        public ResourceViewModel[][] Resources { get; set; }
        private readonly Dictionary<string, ResourceViewModel> ResourcesMap;

        public RoomBookingsViewModel(
                RoomBookingsDateTimes roomBookingsDateTimes, Room room) {

            SelectedDateTime = roomBookingsDateTimes.SelectedDateTime;
            NextDay = roomBookingsDateTimes.NextDay;
            PreviousDay = roomBookingsDateTimes.PreviousDay;
            NextHour = roomBookingsDateTimes.NextHour;
            PreviousHour = roomBookingsDateTimes.PreviousHour;

            ResourceGroup[] resourceGroups = room.ResourceGroups;

            ResourceGroupViewModel[] resourceGroupsViewModels =
                new ResourceGroupViewModel[resourceGroups.Length];

            Resource[][] resources = room.Resources;

            Resources = new ResourceViewModel[resources.Length][];

            ResourcesMap = new Dictionary<string, ResourceViewModel>();

            for (int y = 0; y < Resources.Length; y++) {
                Resources[y] = new ResourceViewModel[resources[y].Length];
            }

            for (int i = 0; i < resourceGroups.Length; i++) {
                ResourceGroup resourceGroup = resourceGroups[i];
                ResourceGroupViewModel resourceGroupViewModel =
                    new ResourceGroupViewModel(resourceGroup);

                resourceGroupsViewModels[i] = resourceGroupViewModel;

                List<ResourceViewModel> selectableResourcesViewModels =
                    resourceGroupViewModel.SelectableResources;

                List<ResourceViewModel> unselectableResourcesViewModels =
                    resourceGroupViewModel.UnselectableResources;

                AddResourcesViewModels(selectableResourcesViewModels);
                AddResourcesViewModels(unselectableResourcesViewModels);
            }
        }

        public void AddBookings(List<RoomBookingViewModel> bookings) {
            foreach(RoomBookingViewModel booking in bookings) {
                foreach (string resourceId in booking.ResourceIds) {
                    ResourcesMap[resourceId].SetBooked(booking);
                }
            }
        }

        private void AddResourcesViewModels(
                List<ResourceViewModel> resources) {

            foreach (ResourceViewModel resource in resources) {
                Resources[resource.BlockY][resource.BlockX] = resource;
                ResourcesMap.Add(resource.Id, resource);
            }
        }

    }
}
