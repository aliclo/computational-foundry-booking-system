﻿using Computational_Foundry_Booking_System.Models.View_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public class Table : Resource {

        private const bool SELECTABLE = false;

        public ResourceExtend ExtendType { get; set; }

        public Table() : base() {

        }

        public Table(int blockX, int blockY, ResourceGroup resourceGroup,
                ResourceExtend extendType) : base(blockX, blockY,
                    Direction.LEFT, resourceGroup) {

            ExtendType = extendType;
        }

        public override ResourceViewModel GetResourceRoomViewModel(
                ResourceGroupViewModel resourceGroup, string groupId,
                bool selected) {

            return new TableViewModel(this, resourceGroup, groupId);
        }

        public override bool IsSelectable() {
            return SELECTABLE;
        }

        public override void AddToResourceGroup(ResourceGroup resourceGroup) {
            resourceGroup.UnselectableResources.Add(this);
        }

    }
}
