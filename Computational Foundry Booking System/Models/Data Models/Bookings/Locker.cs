﻿using Computational_Foundry_Booking_System.Models.View_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public class Locker : Resource {

        private const bool SELECTABLE = true;

        public Locker() : base() {

        }

        public Locker(int blockX, int blockY, ResourceGroup resourceGroup)
                : base(blockX, blockY, Direction.LEFT, resourceGroup) {


        }

        public override ResourceViewModel GetResourceRoomViewModel(
                ResourceGroupViewModel resourceGroup, string groupId,
                bool selected) {

            return new LockerViewModel(this, resourceGroup, groupId, selected);
        }

        public override bool IsSelectable() {
            return SELECTABLE;
        }

        public override void AddToResourceGroup(ResourceGroup resourceGroup) {
            resourceGroup.SelectableResources.Add(this);
        }

    }
}
