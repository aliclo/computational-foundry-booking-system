﻿using Computational_Foundry_Booking_System.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public class ResourceGroup {

        public string Id { get; set; }
        public string RoomId { get; set; }
        
        [NotMapped]
        public List<Resource> SelectableResources { get; set; }

        [NotMapped]
        public List<Resource> UnselectableResources { get; set; }

        public ResourceGroup() {
            SelectableResources = new List<Resource>();
            UnselectableResources = new List<Resource>();
        }

        public async Task Initialise(ApplicationDbContext dbContext) {
            List<Resource> resources = new List<Resource>();

            resources = await dbContext.Resources.Where(
                r => r.ResourceGroupId.Equals(Id) && r.RoomId.Equals(RoomId))
                .ToListAsync();

            SelectableResources = resources.Where(r => r.IsSelectable())
                .ToList();

            UnselectableResources = resources.Where(r => !r.IsSelectable())
                .ToList();
        }

    }
}
