﻿using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.View_Models.Bookings;
using Computational_Foundry_Booking_System.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public class Booking {

        [Key]
        public string Id { get; set; }

        public string Name { get; set; }
        public List<Resource> Resources { get; set; }
        public DateTime BeginDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Description { get; set; }
        public long SharePointCalendarId { get; set; }

        public string UserId { get; set; }
        public IndustryPartner User { get; set; }

        public Booking() {

        }

        public Booking(NewBookingViewModel bookingViewModel,
                IndustryPartner user) {

            this.Name = bookingViewModel.Name;
            this.BeginDateTime = bookingViewModel.BeginDateTime;
            this.EndDateTime = bookingViewModel.EndDateTime;
            this.Description = bookingViewModel.Description;
            this.UserId = user.Id;
            this.User = user;
        }

        public async Task Initialise(NewBookingViewModel bookingViewModel,
                ResourcesRepository resourcesRepository) {

            string[] resourceIds = bookingViewModel.ResourceIds;

            Resources = await resourcesRepository.GetResourcesAsync(
                resourceIds);

            //string[] resourceIds = bookingViewModel.ResourceIds.Split(";");

            //this.Resources = await resourcesRepository.GetResourcesAsync(
            //    resourceIds);
        }

        public async Task Initialise(AppUserRepository appUserRepository,
                ResourcesRepository resourcesRepository) {

            await InitialiseUser(appUserRepository);

            await InitialiseResources(resourcesRepository);
        }

        private async Task InitialiseUser(
                AppUserRepository appUserRepository) {

            User = (IndustryPartner) await
                appUserRepository.GetUserFromIdAsync(UserId);
        }

        private async Task InitialiseResources(
                ResourcesRepository resourcesRepository) {

            Resources = await resourcesRepository.GetResourcesAsync(this);
        }

        public string GetSharePointCalendarItemRepresentation() {
            return "{" +
                "\"Title\": \"" + Name + "\"," +
                "\"EventDate\": \""
                    + BeginDateTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"," +
                "\"EndDate\": \""
                    + EndDateTime.ToString("yyyy-MM-ddTHH:mm:ss") + "\"," +
                "\"Description\": \"" + Description + "\"" +
                "}";
        }

    }
}
