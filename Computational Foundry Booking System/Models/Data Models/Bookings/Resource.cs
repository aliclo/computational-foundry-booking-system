﻿using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.View_Models.Bookings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public abstract class Resource {

        public string Id { get; set; }

        public string RoomId { get; set; }

        public Room Room { get; set; }

        public int BlockX { get; set; }

        public int BlockY { get; set; }

        public Direction Direction { get; set; }

        public string ResourceGroupId { get; set; }

        public ResourceGroup ResourceGroup { get; set; }

        public Resource() {
            
        }

        public Resource(int blockX, int blockY, Direction direction,
                ResourceGroup resourceGroup) {

            BlockX = blockX;
            BlockY = blockY;
            Direction = direction;
            ResourceGroup = resourceGroup;
            AddToResourceGroup(resourceGroup);
        }

        public async void Initialise(ApplicationDbContext dbContext) {
            ResourceGroup = await dbContext.ResourceGroups.SingleAsync(rg =>
                rg.Id.Equals(ResourceGroupId));

            await ResourceGroup.Initialise(dbContext);
        }

        public abstract ResourceViewModel GetResourceRoomViewModel(
            ResourceGroupViewModel resourceGroup, string groupId,
            bool selected);

        public abstract bool IsSelectable();

        public abstract void AddToResourceGroup(ResourceGroup resourceGroup);

    }
}
