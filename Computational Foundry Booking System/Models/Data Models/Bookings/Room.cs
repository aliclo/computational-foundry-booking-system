﻿using Computational_Foundry_Booking_System.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public class Room {

        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

        public int WidthBlocks { get; set; }

        public int HeightBlocks { get; set; }

        [NotMapped]
        public Resource[][] Resources { get; set; }

        [NotMapped]
        public ResourceGroup[] ResourceGroups { get; set; }

        public Room() {

        }

        public Room(string name, ResourceGroup[] resourceGroups) {
            int maxX = 0;
            int maxY = 0;

            foreach(ResourceGroup resourceGroup in resourceGroups) {
                List<Resource> selectableResources =
                    resourceGroup.SelectableResources;

                List<Resource> unselectableResources =
                    resourceGroup.UnselectableResources;

                foreach (Resource resource in selectableResources) {
                    maxX = Math.Max(maxX, resource.BlockX);
                    maxY = Math.Max(maxY, resource.BlockY);
                }

                foreach (Resource resource in unselectableResources) {
                    maxX = Math.Max(maxX, resource.BlockX);
                    maxY = Math.Max(maxY, resource.BlockY);
                }
            }

            Name = name;
            HeightBlocks = maxY+1;
            WidthBlocks = maxX+1;
            ResourceGroups = resourceGroups;

            Resources = new Resource[HeightBlocks][];

            for(int i = 0; i < HeightBlocks; i++) {
                Resources[i] = new Resource[WidthBlocks];
            }

            foreach (ResourceGroup resourceGroup in resourceGroups) {
                List<Resource> selectableResources =
                    resourceGroup.SelectableResources;

                List<Resource> unselectableResources =
                    resourceGroup.UnselectableResources;

                foreach (Resource resource in selectableResources) {
                    Resources[resource.BlockY][resource.BlockX] = resource;
                }

                foreach (Resource resource in unselectableResources) {
                    Resources[resource.BlockY][resource.BlockX] = resource;
                }
            }
        }

        public async Task Initialise(ApplicationDbContext dbContext) {
            Resources = new Resource[HeightBlocks][];

            for(int i = 0; i < HeightBlocks; i++) {
                Resources[i] = new Resource[WidthBlocks];
            }

            ResourceGroups = dbContext.ResourceGroups.Where(
                rg => rg.RoomId.Equals(Id)).ToArray();

            foreach(ResourceGroup resourceGroup in ResourceGroups) {
                await resourceGroup.Initialise(dbContext);

                List<Resource> selectableResources = resourceGroup
                    .SelectableResources;

                List<Resource> unselectableResources = resourceGroup
                    .UnselectableResources;

                foreach (Resource resource in selectableResources) {
                    Resources[resource.BlockY][resource.BlockX] = resource;
                }

                foreach (Resource resource in unselectableResources) {
                    Resources[resource.BlockY][resource.BlockX] = resource;
                }
            }
        }

    }
}
