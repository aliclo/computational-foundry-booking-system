﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Computational_Foundry_Booking_System.Models.View_Models.Bookings;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Bookings {
    public class Chair : Resource {

        private const bool SELECTABLE = true;

        public Chair() : base() {

        }

        public Chair(int blockX, int blockY, Direction dir,
                ResourceGroup resourceGroup) : base(
                    blockX, blockY, dir, resourceGroup) {


        }

        public override ResourceViewModel GetResourceRoomViewModel(
                ResourceGroupViewModel resourceGroup, string groupId,
                bool selected) {

            return new ChairViewModel(this, resourceGroup, groupId, selected);
        }

        public override bool IsSelectable() {
            return SELECTABLE;
        }

        public override void AddToResourceGroup(ResourceGroup resourceGroup) {
            resourceGroup.SelectableResources.Add(this);
        }

    }
}
