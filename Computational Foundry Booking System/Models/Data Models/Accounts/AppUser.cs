﻿using Computational_Foundry_Booking_System.Controllers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Accounts {

    public abstract class AppUser : IdentityUser {

        public abstract bool CanLogin { get; }

        public abstract IActionResult GetHomepage(HomeController controller);

        public abstract string[] GetDefaultClaims();

    }

}
