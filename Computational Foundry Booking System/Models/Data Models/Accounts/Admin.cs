﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Computational_Foundry_Booking_System.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Accounts {

    public class Admin : AppUser {

        private static readonly string[] DEFAULT_CLAIMS = {
            Claims.CAN_APPROVE_ACCOUNTS, Claims.CAN_DENY_ACCOUNTS};

        public override bool CanLogin {
            get {
                return true;
            }
        }

        public override IActionResult GetHomepage(HomeController controller) {
            return controller.View("AdminHomepage");
        }

        public override string[] GetDefaultClaims() {
            return DEFAULT_CLAIMS;
        }

    }

}
