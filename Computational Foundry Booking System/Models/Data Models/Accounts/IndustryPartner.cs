﻿using Computational_Foundry_Booking_System.Controllers;
using Computational_Foundry_Booking_System.Models.View_Models.Accounts;
using Microsoft.AspNetCore.Mvc;

namespace Computational_Foundry_Booking_System.Models.Data_Models.Accounts {

    public class IndustryPartner : AppUser {

        private static readonly string[] DEFAULT_CLAIMS = {
            Claims.CAN_BOOK };

        public bool Approved { get; set; }
        public string CompanyName { get; set; }
        public string WebsiteLink { get; set; }

        public override bool CanLogin {
            get {
                return Approved;
            }
        }

        public IndustryPartner() {

        }

        public IndustryPartner(PartnerRegisterViewModel viewModel) {
            UserName = viewModel.Email;
            Email = viewModel.Email;
            CompanyName = viewModel.CompanyName;
            WebsiteLink = viewModel.WebsiteLink;
            Approved = false;
        }

        public override IActionResult GetHomepage(HomeController controller) {
            return controller.View("PartnerHomepage");
        }

        public override string[] GetDefaultClaims() {
            return DEFAULT_CLAIMS;
        }
    }

}
