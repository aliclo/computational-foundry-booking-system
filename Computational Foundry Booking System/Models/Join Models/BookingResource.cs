﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Models.Join_Models {
    public class BookingResource {

        public string BookingId { get; set; }
        public Booking Booking { get; set; }

        public string ResourceId { get; set; }
        public Resource Resource { get; set; }

        public BookingResource() {

        }

        public BookingResource(Booking booking, Resource resource) {
            this.BookingId = booking.Id;
            this.Booking = booking;
            this.ResourceId = resource.Id;
            this.Resource = resource;
        }

    }
}
