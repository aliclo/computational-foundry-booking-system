﻿using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.View_Models.Accounts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Repositories {
    public class AppUserRepository {

        private readonly ApplicationDbContext _dbContext;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public AppUserRepository(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<SignInResult> SignInAsync(
                LoginViewModel loginViewModel) {

            string username = loginViewModel.Email;

            AppUser user = await GetUserAsync(username);

            if(user == null) {
                return SignInResult.Failed;
            }

            if (user.CanLogin) {
                string password = loginViewModel.Password;

                return await _signInManager.PasswordSignInAsync(
                    username, password, false, true);
            } else {
                return SignInResult.NotAllowed;
            }
        }

        public async Task<IdentityResult> CreateUserAsync(
                AppUser appUser, string password) {

            IdentityResult result = await _userManager.CreateAsync(
                appUser, password);

            if(result.Succeeded) {
                foreach(string claimName in appUser.GetDefaultClaims()) {
                    Claim claim = new Claim(claimName, "");
                    await _userManager.AddClaimAsync(appUser, claim);
                }
            }

            return result;
        }

        public async Task<IdentityResult> UpdateUserAsync(AppUser user) {
            return await _userManager.UpdateAsync(user);
        }

        public async Task<IdentityResult> DeleteUserAsync(AppUser user) {
            return await _userManager.DeleteAsync(user);
        }

        public async Task<AppUser> GetUserAsync(string name) {
            return await _userManager.FindByNameAsync(name);
        }

        public async Task<AppUser> GetUserFromIdAsync(string id) {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<AppUser> GetUserAsync(ClaimsPrincipal user) {
            return await _userManager.GetUserAsync(user);
        }

        public async Task SignOutAsync() {
            await _signInManager.SignOutAsync();
        }

        public async Task<IndustryPartner[]> GetUsersAwaitingApprovalAsync() {
            return await _dbContext.IndustryPartners.Where(p => !p.Approved)
                .ToArrayAsync();
        }

    }
}
