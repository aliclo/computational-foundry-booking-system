﻿using Computational_Foundry_Booking_System.Controllers;
using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Computational_Foundry_Booking_System.Models.Join_Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System.Repositories {
    public class ResourcesRepository {

        private readonly ApplicationDbContext _dbContext;
        private readonly AppUserRepository _appUserRepository;

        public ResourcesRepository(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            this._dbContext = dbContext;
            this._appUserRepository = new AppUserRepository(dbContext,
                userManager, signInManager);
        }

        public async Task<Room> GetRoomAsync() {
            Room room = await _dbContext.Rooms.SingleAsync();

            await room.Initialise(_dbContext);

            return room;
        }

        public async Task<bool> DoesRoomExistAsync() {
            return await _dbContext.Rooms.AnyAsync();
        }

        public bool CheckAllResourcesExist(string[] resourceIds) {
            return resourceIds.All(id => _dbContext.Resources.Any(
                r => r.Id.Equals(id)));
        }

        public async Task AddRoomAsync(Room room) {

            await _dbContext.Rooms.AddAsync(room);

            foreach(ResourceGroup resourceGroup in room.ResourceGroups) {
                resourceGroup.RoomId = room.Id;
                await _dbContext.ResourceGroups.AddAsync(resourceGroup);

                List<Resource> selectableResources =
                    resourceGroup.SelectableResources;

                List<Resource> unselectableResources =
                    resourceGroup.UnselectableResources;

                await AddGroupResources(room, resourceGroup,
                    selectableResources);

                await AddGroupResources(room, resourceGroup,
                    unselectableResources);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Resource>> GetResourcesAsync(
                string[] resourceIds) {

            return await _dbContext.Resources.Where(
                r => resourceIds.Any(id => id.Equals(r.Id))).ToListAsync();
        }

        public async Task<List<Resource>> GetResourcesAsync(Booking booking) {
            IQueryable<BookingResource> bookingResource =
                _dbContext.BookingResources.Where(
                    br => br.BookingId.Equals(booking.Id));

            return await _dbContext.Resources.Where(r => bookingResource.Any(
                br => br.ResourceId.Equals(r.Id))).ToListAsync();
        }

        public async Task<int> GetNumBookedChairs(Booking booking) {
            IQueryable<BookingResource> bookingResources =
                _dbContext.BookingResources.Where(br =>
                br.BookingId.Equals(booking.Id));

            return await _dbContext.Chairs.Where(c => bookingResources.Any(
                br => br.ResourceId.Equals(c.Id))).CountAsync();
        }

        public async Task<int> GetNumBookedLockers(Booking booking) {
            IQueryable<BookingResource> bookingResources =
                _dbContext.BookingResources.Where(br =>
                br.BookingId.Equals(booking.Id));

            return await _dbContext.Lockers.Where(c => bookingResources.Any(
                br => br.ResourceId.Equals(c.Id))).CountAsync();
        }

        public async Task AddBookingAsync(Booking booking) {
            await _dbContext.Bookings.AddAsync(booking);

            foreach(Resource resource in booking.Resources) {
                BookingResource bookingResource =
                    new BookingResource(booking, resource);

                await _dbContext.BookingResources.AddAsync(bookingResource);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Booking>> GetWeekBookingsAsync(
                DateTime weekStartDate) {

            DateTime weekEndDate = weekStartDate.AddDays(
                BookingsController.DAYS_IN_WEEK);

            List<Booking> bookings = await _dbContext.Bookings.Where(b =>
                !((b.BeginDateTime < weekStartDate
                && b.EndDateTime < weekStartDate) ||
                (b.BeginDateTime >= weekEndDate
                && b.EndDateTime >= weekEndDate))).ToListAsync();

            foreach(Booking booking in bookings) {
                await booking.Initialise(_appUserRepository, this);
            }

            return bookings;
        }

        public async Task<List<Booking>> GetRoomBookingsAsync(
                DateTime startDateTime, DateTime endDateTime) {

            List<Booking> bookings = await _dbContext.Bookings.Where(b =>
                !(b.BeginDateTime < startDateTime &&
                b.EndDateTime <= startDateTime) &&
                !(b.BeginDateTime >= endDateTime &&
                b.EndDateTime > endDateTime)).ToListAsync();

            foreach(Booking booking in bookings) {
                await booking.Initialise(_appUserRepository, this);
            }

            return bookings;
        }

        private async Task AddGroupResources(Room room,
                ResourceGroup resourceGroup, List<Resource> resources) {

            foreach (Resource resource in resources) {
                resource.RoomId = room.Id;
                resource.ResourceGroupId = resourceGroup.Id;
                await _dbContext.Resources.AddAsync(resource);
            }
        }

    }
}
