﻿using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Computational_Foundry_Booking_System {
    public class SharePointHelper {

        private const int SERVICE_UNAVAILABLE_STATUS_CODE = 503;

        public static async Task<StatusCodeResult> SharePointCreateBooking(
                Controller controller, HttpClient client, Booking booking) {

            //Get context information which contains the token required
            //to create bookings
            string contextInfoUrl =
                "http://win-gkgel6bbp2n/sites/roombookings/_api/contextinfo";

            HttpResponseMessage response = null;

            try {
                response = await client.PostAsync(contextInfoUrl, null);
            } catch(HttpRequestException e) {
                return controller.StatusCode(SERVICE_UNAVAILABLE_STATUS_CODE);
            }

            if (!response.IsSuccessStatusCode) {
                return controller.StatusCode(SERVICE_UNAVAILABLE_STATUS_CODE);
            }

            string contextStr = await response.Content.ReadAsStringAsync();
            XmlDocument contextDocument = new XmlDocument();
            contextDocument.LoadXml(contextStr);

            //Get token from context
            XmlNodeList formTokenNode = contextDocument.GetElementsByTagName(
                "d:FormDigestValue");

            string formToken = formTokenNode[0].InnerText;

            //Add token to header
            client.DefaultRequestHeaders.Add("X-RequestDigest", formToken);

            //Booking to be added to SharePoint calendar (in JSON format)
            string contentString =
                booking.GetSharePointCalendarItemRepresentation();

            StringContent content = new StringContent(contentString,
                Encoding.UTF8, "application/json");

            string roomBookingItemsUrl = "http://win-gkgel6bbp2n/sites/" +
                "roombookings/_api/lists/getbytitle('Room%20Booking')/items";

            //Post new booking
            response = await client.PostAsync(roomBookingItemsUrl, content);

            if (!response.IsSuccessStatusCode) {
                return controller.StatusCode((int)response.StatusCode);
            }

            //Response provides the id of the newly added booking
            string responseStr = await response.Content.ReadAsStringAsync();

            XmlDocument responseDocument = new XmlDocument();
            responseDocument.LoadXml(responseStr);

            //Retrieve id of added booking and store it
            //(to refer for updating or deleting)
            XmlNodeList idNode = responseDocument.GetElementsByTagName("d:Id");

            long id = Convert.ToInt64(idNode[0].InnerText);

            booking.SharePointCalendarId = id;

            return controller.Ok();
        }

    }
}
