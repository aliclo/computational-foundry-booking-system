﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System {
    public class RoomBookingsDateTimes {

        public DateTime SelectedDateTime { get; set; }
        public DateTime NextDay { get; set; }
        public DateTime PreviousDay { get; set; }
        public DateTime NextHour { get; set; }
        public DateTime PreviousHour { get; set; }

        public RoomBookingsDateTimes(DateTime selectedDateTime,
                DateTime nextDay, DateTime previousDay, DateTime nextHour,
                DateTime previousHour) {

            SelectedDateTime = selectedDateTime;
            NextDay = nextDay;
            PreviousDay = previousDay;
            NextHour = nextHour;
            PreviousHour = previousHour;
        }

    }
}
