﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Computational_Foundry_Booking_System.Authorization;
using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.View_Models.Accounts;
using Computational_Foundry_Booking_System.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Computational_Foundry_Booking_System.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        public const string FAILED_TO_LOGIN_ERR_MSG
            = "Couldn't register account";

        private readonly AppUserRepository _appUserRepository;

        public AccountController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext,
                userManager, signInManager);
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult Login(string returnUrl) {
            LoginViewModel loginViewModel = new LoginViewModel() {
                ReturnUrl = returnUrl
            };

            return View(loginViewModel);
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel) {
            if(!ModelState.IsValid) {
                return View(loginViewModel);
            }

            SignInResult result = await _appUserRepository.SignInAsync(
                loginViewModel);

            if(result.IsNotAllowed) {
                ModelState.AddModelError("", "Not allowed to login");
                return View(loginViewModel);
            }

            if (result.IsLockedOut) {
                ModelState.AddModelError("", "Account is locked");
                return View(loginViewModel);
            }

            if (!result.Succeeded) {
                ModelState.AddModelError("", "Failed to login");
                return View(loginViewModel);
            }

            if (loginViewModel.ReturnUrl == null) {
                return RedirectToAction("Index", "Home");
            } else {
                return LocalRedirect(loginViewModel.ReturnUrl);
            }
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult Register() {
            return View();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(
                PartnerRegisterViewModel registerViewModel) {

            if(!ModelState.IsValid) {
                return View(registerViewModel);
            }

            IndustryPartner industryPartner = new IndustryPartner(
                registerViewModel);

            IdentityResult result = await _appUserRepository.CreateUserAsync(
                industryPartner, registerViewModel.Password);

            if(!result.Succeeded) {
                ModelState.AddModelError("", "Couldn't register account");

                foreach(IdentityError error in result.Errors) {
                    ModelState.AddModelError("", error.Description);
                }

                return View(registerViewModel);
            }

            registerViewModel = new PartnerRegisterViewModel();

            registerViewModel.Success = true;

            return View(registerViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Logout() {
            await _appUserRepository.SignOutAsync();

            return RedirectToAction("Login");
        }

        [HttpGet]
        public IActionResult AccessDenied() {
            return View();
        }

    }
}