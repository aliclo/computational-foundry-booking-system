﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Computational_Foundry_Booking_System.Models;
using Microsoft.AspNetCore.Authorization;
using Computational_Foundry_Booking_System.Repositories;
using Computational_Foundry_Booking_System.Data;
using Microsoft.AspNetCore.Identity;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.View_Models.Accounts;

namespace Computational_Foundry_Booking_System.Controllers {

    [Authorize]
    public class HomeController : Controller {

        private readonly AppUserRepository _appUserRepository;

        public HomeController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext,
                userManager, signInManager);
        }

        [HttpGet]
        public async Task<IActionResult> Index() {
            AppUser appUser = await _appUserRepository.GetUserAsync(User);

            return appUser.GetHomepage(this);
        }

        [HttpGet]
        [Authorize(Policy = Claims.CAN_ACCESS_ACCOUNT_APPROVAL)]
        public async Task<IActionResult> AccountApproval() {
            IndustryPartner[] users = await _appUserRepository
                .GetUsersAwaitingApprovalAsync();

            AccountApprovalViewModel accountApprovalViewModel =
                new AccountApprovalViewModel(users);

            return View(accountApprovalViewModel);
        }

        [HttpPost]
        [Authorize(Policy = Claims.CAN_APPROVE_ACCOUNTS)]
        public async Task<IActionResult> ApproveAccount(string accountEmail) {
            IndustryPartner user = (IndustryPartner) await
                _appUserRepository.GetUserAsync(accountEmail);

            user.Approved = true;

            IdentityResult result = await _appUserRepository.UpdateUserAsync(
                user);

            if(result.Succeeded) {
                return Ok();
            } else {
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize(Policy = Claims.CAN_DENY_ACCOUNTS)]
        public async Task<IActionResult> DenyAccount(string accountEmail) {
            IndustryPartner user = (IndustryPartner)await
                _appUserRepository.GetUserAsync(accountEmail);

            IdentityResult result = await _appUserRepository.DeleteUserAsync(
                user);

            if (result.Succeeded) {
                return Ok();
            } else {
                return BadRequest();
            }
        }

        public IActionResult Privacy() {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

}
