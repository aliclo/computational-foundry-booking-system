﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Computational_Foundry_Booking_System.Data;
using Computational_Foundry_Booking_System.Models.Data_Models.Accounts;
using Computational_Foundry_Booking_System.Models.Data_Models.Bookings;
using Computational_Foundry_Booking_System.Models.View_Models.Bookings;
using Computational_Foundry_Booking_System.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Computational_Foundry_Booking_System.Controllers {

    [Authorize]
    public class BookingsController : Controller {

        public const int DAYS_IN_WEEK = 7;
        private const DayOfWeek END_DAY = DayOfWeek.Friday;
        private const int WEEKEND_SPACING = (7 - (int) END_DAY)+1;
        private const int WEEK_DAYS_AVAILABLE = ((int) END_DAY - 1);
        private const int START_HOUR = 9;
        private const int END_HOUR = 17;
        private const int HOURS_SPACING = END_HOUR - START_HOUR;
        private const int MIN_SLOT_DURATION_HOURS = 1;

        private const int END_HOUR_START = START_HOUR
            + MIN_SLOT_DURATION_HOURS;

        private const int END_HOUR_END = END_HOUR + MIN_SLOT_DURATION_HOURS;

        private static readonly string[] DAYS_STR = { "Sun", "Mon", "Tue",
            "Wed", "Thu", "Fri", "Sat" };

        private static readonly HttpClientHandler clientHandler =
                new HttpClientHandler() {

            Credentials = new NetworkCredential(
                "cossharepoint\\administrator", "Admin!2019@vm")
        };

        private readonly HttpClient client = new HttpClient(clientHandler);

        private readonly AppUserRepository _appUserRepository;
        private readonly ResourcesRepository _resourcesRepository;

        public BookingsController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext,
                userManager, signInManager);

            _resourcesRepository = new ResourcesRepository(dbContext,
                userManager, signInManager);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> WeeklyBookings() {
            DateTime weekStartDate = DateTime.Today;
            int dayOfThisWeek = (int) weekStartDate.DayOfWeek;
            TimeSpan daysFromSunday = new TimeSpan(dayOfThisWeek, 0, 0, 0);
            DateTime sundayDate = weekStartDate.Subtract(daysFromSunday);
            DateTime mondayDate = sundayDate.AddDays((int) DayOfWeek.Monday);

            List<Booking> weekBookings = await _resourcesRepository
                .GetWeekBookingsAsync(mondayDate);

            List<WeeklyBookingViewModel> weekBookingsViewModels;

            if(User.Identity.IsAuthenticated) {
                AppUser user = await _appUserRepository.GetUserAsync(User);

                weekBookingsViewModels = await AddWeeklyBookingsViewModels(
                    weekBookings, user);
            } else {
                weekBookingsViewModels = await AddWeeklyBookingsViewModels(
                    weekBookings);
            }

            WeeklyBookingsViewModel weeklyBookingsViewModel =
                    new WeeklyBookingsViewModel(mondayDate, END_DAY,
                    START_HOUR, END_HOUR);

            weeklyBookingsViewModel.AddBookings(weekBookingsViewModels);

            return View(weeklyBookingsViewModel);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> WeekBookings(string dateTime) {
            DateTime dateTimeParsed = DateTime.Parse(dateTime);

            int dayOfThisWeek = (int)dateTimeParsed.DayOfWeek;
            TimeSpan daysFromSunday = new TimeSpan(dayOfThisWeek, 0, 0, 0);
            DateTime sundayDate = dateTimeParsed.Subtract(daysFromSunday);
            DateTime mondayDate = sundayDate.AddDays((int) DayOfWeek.Monday);

            List<Booking> weekBookings = await _resourcesRepository
                .GetWeekBookingsAsync(mondayDate);

            List<WeeklyBookingViewModel> weekBookingsViewModels;

            if (User.Identity.IsAuthenticated) {
                AppUser user = await _appUserRepository.GetUserAsync(User);

                weekBookingsViewModels = await AddWeeklyBookingsViewModels(
                    weekBookings, user);
            } else {
                weekBookingsViewModels = await AddWeeklyBookingsViewModels(
                    weekBookings);
            }

            WeeklyBookingsViewModel weeklyBookingsViewModel =
                new WeeklyBookingsViewModel(mondayDate, END_DAY,
                START_HOUR, END_HOUR);

            weeklyBookingsViewModel.AddBookings(weekBookingsViewModels);

            return View("WeeklyBookings", weeklyBookingsViewModel);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> CurrentRoomBookings() {
            DateTime dateTime = DateTime.Now;
            DateTime dateTimeEnd = dateTime.AddHours(
                MIN_SLOT_DURATION_HOURS);

            RoomBookingsDateTimes roomBookingsDateTimes =
                SetupRoomBookingsDateTimes(dateTime);

            Room room = await _resourcesRepository.GetRoomAsync();

            List<Booking> roomBookings = await _resourcesRepository
                .GetRoomBookingsAsync(dateTime, dateTimeEnd);

            List<RoomBookingViewModel> roomBookingsViewModels;

            if (User.Identity.IsAuthenticated) {
                AppUser user = await _appUserRepository.GetUserAsync(User);

                roomBookingsViewModels = AddRoomBookingsViewModels(
                    roomBookings, user);
            } else {
                roomBookingsViewModels = AddRoomBookingsViewModels(
                    roomBookings);
            }

            RoomBookingsViewModel roomBookingsViewModel =
                new RoomBookingsViewModel(roomBookingsDateTimes, room);

            roomBookingsViewModel.AddBookings(roomBookingsViewModels);

            return View("RoomBookings", roomBookingsViewModel);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> RoomBookings(string dateTime) {
            DateTime dateTimeParsed = DateTime.Parse(dateTime);
            DateTime dateTimeEnd = dateTimeParsed.AddHours(
                MIN_SLOT_DURATION_HOURS);

            RoomBookingsDateTimes roomBookingsDateTimes =
                SetupRoomBookingsDateTimes(dateTimeParsed);

            Room room = await _resourcesRepository.GetRoomAsync();

            List<Booking> roomBookings = await _resourcesRepository
                .GetRoomBookingsAsync(dateTimeParsed, dateTimeEnd);

            List<RoomBookingViewModel> roomBookingsViewModels;

            if (User.Identity.IsAuthenticated) {
                AppUser user = await _appUserRepository.GetUserAsync(User);

                roomBookingsViewModels = AddRoomBookingsViewModels(
                    roomBookings, user);
            } else {
                roomBookingsViewModels = AddRoomBookingsViewModels(
                    roomBookings);
            }

            RoomBookingsViewModel roomBookingsViewModel =
                new RoomBookingsViewModel(roomBookingsDateTimes, room);

            roomBookingsViewModel.AddBookings(roomBookingsViewModels);

            return View(roomBookingsViewModel);
        }

        private RoomBookingsDateTimes SetupRoomBookingsDateTimes(
                DateTime selectedDateTime) {

            DayOfWeek dayOfThisWeek = selectedDateTime.DayOfWeek;
            int hour = selectedDateTime.Hour;

            if (hour < START_HOUR) {
                selectedDateTime = selectedDateTime.AddHours(
                    START_HOUR - hour);

            } else if (hour > END_HOUR) {
                selectedDateTime = selectedDateTime.AddHours(
                    END_HOUR - hour);
            }

            if (dayOfThisWeek < DayOfWeek.Monday) {
                selectedDateTime = selectedDateTime.AddDays(
                    DayOfWeek.Monday - dayOfThisWeek);

            } else if (dayOfThisWeek > END_DAY) {
                selectedDateTime = selectedDateTime.AddDays(
                    END_DAY - dayOfThisWeek);
            }

            dayOfThisWeek = selectedDateTime.DayOfWeek;
            hour = selectedDateTime.Hour;

            DateTime nextDay;
            DateTime previousDay;
            DateTime nextHour;
            DateTime previousHour;

            if (dayOfThisWeek == END_DAY) {
                nextDay = selectedDateTime.AddDays(WEEKEND_SPACING);
                previousDay = selectedDateTime.AddDays(-1);
            } else {
                nextDay = selectedDateTime.AddDays(1);

                if (dayOfThisWeek == DayOfWeek.Monday) {
                    previousDay = selectedDateTime.AddDays(-WEEKEND_SPACING);
                } else {
                    previousDay = selectedDateTime.AddDays(-1);
                }
            }

            if (hour == END_HOUR) {
                nextHour = selectedDateTime.AddHours(-HOURS_SPACING);
                previousHour = selectedDateTime.AddHours(-1);
            } else {
                nextHour = selectedDateTime.AddHours(1);

                if (hour == START_HOUR) {
                    previousHour = selectedDateTime.AddHours(HOURS_SPACING);
                } else {
                    previousHour = selectedDateTime.AddHours(-1);
                }
            }

            return new RoomBookingsDateTimes(selectedDateTime, nextDay,
                previousDay, nextHour, previousHour);
        }

        [HttpPost]
        [Authorize(Policy = Claims.CAN_BOOK)]
        public IActionResult MakeBookingForm(
                ResourcesSelectionViewModel resourcesSelection) {

            if(resourcesSelection.ResourceIds == null) {
                string dateTime = resourcesSelection.SelectedDateTime
                    .ToString("dd-MM-yyyy HH:00");

                return RedirectToAction("RoomBookings", "Bookings",
                    new { dateTime });
            }

            NewBookingViewModel newBooking = new NewBookingViewModel(
                resourcesSelection);

            BookingFormViewModel bookingForm =
                new BookingFormViewModel(newBooking, END_DAY, START_HOUR,
                END_HOUR);

            return View("MakeBooking", bookingForm);
        }

        [HttpPost]
        [Authorize(Policy = Claims.CAN_BOOK)]
        public async Task<IActionResult> MakeBooking(
                NewBookingViewModel bookingViewModel) {

            IActionResult result = ValidateBooking(bookingViewModel);

            if(result != null) {
                return result;
            }

            //Need to check if logged in user is industry partner
            IndustryPartner user = (IndustryPartner) await
                _appUserRepository.GetUserAsync(User);

            Booking booking = new Booking(bookingViewModel, user);
            await booking.Initialise(bookingViewModel, _resourcesRepository);
            
            StatusCodeResult status =
                await SharePointHelper.SharePointCreateBooking(this,
                client, booking);

            if(status.StatusCode != (int) HttpStatusCode.OK) {
                return status;
            }

            await _resourcesRepository.AddBookingAsync(booking);

            return RedirectToAction("Index", "Home");
        }

        [HttpDelete]
        public async Task<StatusCodeResult> CancelBooking(Booking booking) {
            //Doesn't seem to require a token for deleting bookings

            /*string contextInfoUrl =
                "http://win-gkgel6bbp2n/sites/roombookings/_api/contextinfo";

            HttpResponseMessage response = await client.PostAsync(
                contextInfoUrl, null);

            string contextStr = await response.Content.ReadAsStringAsync();
            XmlDocument contextDocument = new XmlDocument();
            contextDocument.LoadXml(contextStr);
            XmlNodeList formTokenNode = contextDocument.GetElementsByTagName(
                "d:FormDigestValue");

            string formToken = formTokenNode[0].InnerText;

            client.DefaultRequestHeaders.Add("X-RequestDigest", formToken);*/

            //Doesn't need to check eTag (no precondition)
            client.DefaultRequestHeaders.Add("if-match", "*");

            //Construct URL to referred booking resource
            string roomBookingItemUrl = "http://win-gkgel6bbp2n/sites/" +
                "roombookings/_api/lists/getbytitle('Room%20Booking')/" +
                "items(" + booking.SharePointCalendarId + ")";

            HttpResponseMessage response = await client.DeleteAsync(
                roomBookingItemUrl);

            if (!response.IsSuccessStatusCode) {
                return StatusCode((int) response.StatusCode);
            }

            return Ok();
        }

        [HttpPut]
        public async Task<StatusCodeResult> UpdateBooking(Booking booking) {
            //Doesn't seem to require a token for updating bookings

            /*string contextInfoUrl =
                "http://win-gkgel6bbp2n/sites/roombookings/_api/contextinfo";

            HttpResponseMessage response = await client.PostAsync(
                contextInfoUrl, null);

            string contextStr = await response.Content.ReadAsStringAsync();
            XmlDocument contextDocument = new XmlDocument();
            contextDocument.LoadXml(contextStr);
            XmlNodeList formTokenNode = contextDocument.GetElementsByTagName(
                "d:FormDigestValue");

            string formToken = formTokenNode[0].InnerText;

            client.DefaultRequestHeaders.Add("X-RequestDigest", formToken);*/

            //Doesn't need to check eTag (no precondition)
            client.DefaultRequestHeaders.Add("if-match", "*");

            //Booking to be added to SharePoint calendar (in JSON format)
            string contentString =
                booking.GetSharePointCalendarItemRepresentation();

            StringContent content = new StringContent(contentString,
                Encoding.UTF8, "application/json");

            //Construct URL to referred booking resource
            string roomBookingItemUrl = "http://win-gkgel6bbp2n/sites/" +
                "roombookings/_api/lists/getbytitle('Room%20Booking')/" +
                "items(" + booking.SharePointCalendarId + ")";

            HttpResponseMessage response = await client.PatchAsync(
                roomBookingItemUrl, content);

            if (!response.IsSuccessStatusCode) {
                return StatusCode((int) response.StatusCode);
            }

            return Ok();
        }

        private IActionResult ValidateBooking(NewBookingViewModel booking) {
            bool valid = ModelState.IsValid;

            valid = ValidateBookingDates(booking) && valid;

            valid = ValidateBookingResources(booking) && valid;

            if (!valid) {
                BookingFormViewModel bookingForm =
                    new BookingFormViewModel(booking, END_DAY, START_HOUR,
                    END_HOUR);

                return View(bookingForm);
            }

            return null;
        }

        private bool ValidateBookingDates(NewBookingViewModel booking) {
            bool valid = true;

            DateTime currentDateTime = DateTime.Now;

            if (booking.BeginDateTime < currentDateTime) {
                valid = false;
                ModelState.AddModelError("", "Start date and time has passed");
            }

            if (booking.EndDateTime < currentDateTime) {
                valid = false;
                ModelState.AddModelError("", "End date and time has passed");
            }

            if (booking.BeginDateTime >= booking.EndDateTime) {
                valid = false;
                ModelState.AddModelError("",
                    "Start date and time isn't before end date and time");
            }

            if (booking.BeginDateTime.DayOfWeek < DayOfWeek.Monday ||
                    booking.BeginDateTime.DayOfWeek > END_DAY) {

                valid = false;
                ModelState.AddModelError("", "Start date is outside Mon-" +
                    DAYS_STR[(int)END_DAY]);
            }

            if (booking.BeginDateTime.Hour < START_HOUR ||
                booking.BeginDateTime.Hour > END_HOUR) {

                valid = false;
                ModelState.AddModelError("", "Start time is outside "
                    + START_HOUR + "-" + END_HOUR);
            }

            if (booking.EndDateTime.DayOfWeek < DayOfWeek.Monday ||
                booking.EndDateTime.DayOfWeek > END_DAY) {

                valid = false;
                ModelState.AddModelError("", "End date is outside Mon-" +
                    DAYS_STR[(int)END_DAY]);
            }

            if (booking.EndDateTime.Hour < END_HOUR_START ||
                booking.EndDateTime.Hour > END_HOUR_END) {

                valid = false;
                ModelState.AddModelError("", "End time is outside "
                    + END_HOUR_START + "-" + END_HOUR_END);
            }

            return valid;
        }

        private bool ValidateBookingResources(NewBookingViewModel booking) {
            bool valid = true;

            if(booking.ResourceIds == null) {
                ModelState.AddModelError("", "No resources given");
                return false;
            }

            bool allResourcesExist = _resourcesRepository
                .CheckAllResourcesExist(booking.ResourceIds);

            if (!allResourcesExist) {
                valid = false;
                ModelState.AddModelError("", "Chosen resources don't exist");
            }

            /*string[] resourceIdsArr = booking.ResourceIds.Split(";");

            bool allResourcesExist = _resourcesRepository
                .CheckAllResourcesExist(resourceIdsArr);

            if(!allResourcesExist) {
                valid = false;
                ModelState.AddModelError("", "Chosen resources don't exist");
            }*/

            return valid;
        }

        private async Task<List<WeeklyBookingViewModel>>
                AddWeeklyBookingsViewModels(List<Booking> bookings) {

            List<WeeklyBookingViewModel> viewModelList =
                new List<WeeklyBookingViewModel>();

            foreach(Booking booking in bookings) {
                WeeklyBookingViewModel bookingViewModel =
                    new WeeklyBookingViewModel(booking, false);

                await bookingViewModel.Initialise(
                    booking, _resourcesRepository);

                viewModelList.Add(bookingViewModel);
            }

            return viewModelList;
        }

        private async Task<List<WeeklyBookingViewModel>>
                AddWeeklyBookingsViewModels(List<Booking> bookings,
                AppUser user) {

            List<WeeklyBookingViewModel> viewModelList =
                new List<WeeklyBookingViewModel>();

            foreach (Booking booking in bookings) {
                bool userBooking = booking.UserId.Equals(user.Id);

                WeeklyBookingViewModel bookingViewModel =
                    new WeeklyBookingViewModel(booking, userBooking);

                await bookingViewModel.Initialise(
                    booking, _resourcesRepository);

                viewModelList.Add(bookingViewModel);
            }

            return viewModelList;
        }

        private List<RoomBookingViewModel> AddRoomBookingsViewModels(
                List<Booking> bookings) {

            List<RoomBookingViewModel> viewModelList =
                new List<RoomBookingViewModel>();

            foreach (Booking booking in bookings) {
                viewModelList.Add(new RoomBookingViewModel(booking, false));
            }

            return viewModelList;
        }

        private List<RoomBookingViewModel> AddRoomBookingsViewModels(
                List<Booking> bookings, AppUser user) {

            List<RoomBookingViewModel> viewModelList =
                new List<RoomBookingViewModel>();

            foreach (Booking booking in bookings) {
                bool userBooking = booking.UserId.Equals(user.Id);
                viewModelList.Add(new RoomBookingViewModel(
                    booking, userBooking));
            }

            return viewModelList;
        }

    }
}