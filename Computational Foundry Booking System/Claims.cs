﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Computational_Foundry_Booking_System {
    public class Claims {

        public const string CAN_BOOK = "CAN_BOOK";
        public const string CAN_APPROVE_ACCOUNTS = "CAN_APPROVE_ACCOUNTS";
        public const string CAN_DENY_ACCOUNTS = "CAN_DENY_ACCOUNTS";
        public const string CAN_ACCESS_ACCOUNT_APPROVAL =
            "CAN_ACCESS_ACCOUNT_APPROVAL";

        public static readonly string[] CLAIMS_ARR = { CAN_BOOK,
            CAN_APPROVE_ACCOUNTS, CAN_DENY_ACCOUNTS };

        public static readonly OrClaim[] CLAIM_REQUIREMENTS_ARR = {
            new OrClaim(CAN_BOOK, CAN_BOOK),
            new OrClaim(CAN_APPROVE_ACCOUNTS, CAN_APPROVE_ACCOUNTS),
            new OrClaim(CAN_DENY_ACCOUNTS, CAN_DENY_ACCOUNTS),
            new OrClaim(CAN_ACCESS_ACCOUNT_APPROVAL, CAN_APPROVE_ACCOUNTS,
                CAN_DENY_ACCOUNTS)
        };

        public class OrClaim {
            public string Name { get; set; }
            public string[] Requirements { get; set; }

            public OrClaim(string name, params string[] requirements) {
                Name = name;
                Requirements = requirements;
            }

        }

    }
}
